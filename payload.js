//Raise Ticket Payload
let response = {
	payload : {
	"title": "string",
 	 "description": "string",
 	 "issue_observed_on": "string",
  	"impact": "string",
  	"urgency": "string",
  	"department": "string",
  	"issue": "string",
  	"attachment": "string"
	},
	user_id
}

//Update Ticket Payload
let response = {
    payload : {
    "title": "string",
    "description": "string",
    "issue_observed_on": "string",
    "impact": "string",
    "urgency": "string",
    "department": "string",
    "issue": "string",
    "attachment": "string"
    },
    user_id,
    ticket_id
}

//User Add Comment Payload
let response = {
    payload : {
    "comment": "string"
    },
    user_id,
    ticket_id
}

//Agent Add Comment Payload
let response = {
    payload : {
    "comment": "string"
    },
    agent_id,
    ticket_id
}


//Agent Change Ticket Status Payload
let response = {
    payload : {
    "status": "string",
    "notes" : "string"
    },
    agent_id,
    ticket_id
}
