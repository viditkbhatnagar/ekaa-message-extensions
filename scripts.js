var axios = require("axios");
var data = JSON.stringify({
  chatType: "group",
  topic: "API Called from Postman Trial",
  members: [
    {
      "@odata.type": "#microsoft.graph.aadUserConversationMember",
      roles: ["owner"],
      "user@odata.bind":
        "https://graph.microsoft.com/v1.0/users('fd39393d-5e20-4e63-93d8-b3aef545d062')",
    },
    {
      "@odata.type": "#microsoft.graph.aadUserConversationMember",
      roles: ["owner"],
      "user@odata.bind":
        "https://graph.microsoft.com/v1.0/users('016e04b2-078d-4d84-80e5-1a59be585afd')",
    },
    {
      "@odata.type": "#microsoft.graph.aadUserConversationMember",
      roles: ["owner"],
      "user@odata.bind":
        "https://graph.microsoft.com/v1.0/users('6f4658c6-b364-47ba-b75a-e9108b7cfca0')",
    },
    {
      "@odata.type": "#microsoft.graph.aadUserConversationMember",
      roles: ["owner"],
      "user@odata.bind":
        "https://graph.microsoft.com/v1.0/users('f09ebf74-18e2-445c-9d6d-b092633fb77c')",
    },
  ],
});

var config = {
  method: "post",
  url: "https://graph.microsoft.com/v1.0/chats",
  headers: {
    Authorization:
      "Bearer eyJ0eXAiOiJKV1QiLCJub25jZSI6ImRNV0pPM3hvWmRGTVpRbHVRb0hSeVdxYXdFODMzUXBIRC1PekpiQm84WXciLCJhbGciOiJSUzI1NiIsIng1dCI6Ik1yNS1BVWliZkJpaTdOZDFqQmViYXhib1hXMCIsImtpZCI6Ik1yNS1BVWliZkJpaTdOZDFqQmViYXhib1hXMCJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9jODgyYmNmMS1hOGM3LTQyOTUtODZlZC00MTY4OTcwMzVmZmQvIiwiaWF0IjoxNjQyNjU3NzM3LCJuYmYiOjE2NDI2NTc3MzcsImV4cCI6MTY0MjY2MjY2NywiYWNjdCI6MSwiYWNyIjoiMSIsImFpbyI6IkFYUUFpLzhUQUFBQWM0K1dZcHdXSWUvNDJERTcyWGtWTGFueW5VVWJtTUZ4QWVlRWRiWFRKWEk5Rk4zYVEvV3pzS1hoaWdzYWdrT09oM0Y2dmkzcnpXZEttd0kyakNBaUhVSEVjcXRXd3Z6U1VXdmJpa2tXU05WU1JWS1ZwQmdFeU9aUUN0SCtOQ1hkckRJdjZTeUxIWVhSa1JFNjVyNGZhZz09IiwiYWx0c2VjaWQiOiI1OjoxMDAzMjAwMTE1QkFCODExIiwiYW1yIjpbInB3ZCIsInJzYSJdLCJhcHBfZGlzcGxheW5hbWUiOiJEZXYtRWthYS1DaGF0Ym90IiwiYXBwaWQiOiIzNDcwMTVlOC02NDFmLTQ1MWItYmM2MS0zNWU2NDZhMGQ5MWQiLCJhcHBpZGFjciI6IjEiLCJlbWFpbCI6ImhhcnNoaXQuc2hhcm1hQGNlbGViYWx0ZWNoLmNvbSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0L2U0ZTM0MDM4LWVhMWYtNDg4Mi1iNmU4LWNjZDc3NjQ1OWNhMC8iLCJpZHR5cCI6InVzZXIiLCJpcGFkZHIiOiIyMjMuMTg4LjY2LjI2IiwibmFtZSI6IkhhcnNoaXQgU2hhcm1hIiwib2lkIjoiYmU4ZWQ5OGEtOWY3MS00YWQwLThmNDEtODg4NGVhMDhhYWM5IiwicGxhdGYiOiIzIiwicHVpZCI6IjEwMDMyMDAxQTVEMTQ3MjgiLCJyaCI6IjAuQVQ0QThieUN5TWVvbFVLRzdVRm9sd05mX2VnVmNEUWZaQnRGdkdFMTVrYWcyUjAtQUFvLiIsInNjcCI6Ikdyb3VwLlJlYWQuQWxsIEdyb3VwLlJlYWRXcml0ZS5BbGwgVXNlci5SZWFkIHByb2ZpbGUgb3BlbmlkIGVtYWlsIiwic2lnbmluX3N0YXRlIjpbImttc2kiXSwic3ViIjoiRkd3VEJKQlBndkYtRGo0UEpETGRQaFJsSHNHNy1IYWw3V1JMNDdBTHJwdyIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJBUyIsInRpZCI6ImM4ODJiY2YxLWE4YzctNDI5NS04NmVkLTQxNjg5NzAzNWZmZCIsInVuaXF1ZV9uYW1lIjoiaGFyc2hpdC5zaGFybWFAY2VsZWJhbHRlY2guY29tIiwidXRpIjoiNk9INHhxMDdra1N6cW83cnBtNFBBQSIsInZlciI6IjEuMCIsIndpZHMiOlsiOWI4OTVkOTItMmNkMy00NGM3LTlkMDItYTZhYzJkNWVhNWMzIiwiMTNiZDFjNzItNmY0YS00ZGNmLTk4NWYtMThkM2I4MGYyMDhhIl0sInhtc19zdCI6eyJzdWIiOiJZRVRvVUM0RnM1OG5ibFAtTmo0S0NWbWM3bVliOUYzbUZYVGRLQ3dYWTA4In0sInhtc190Y2R0IjoxNTY1OTU5MTYzfQ.p0oa2Z7XX50-Ji4fhmfVNYcRdtm9TE3K_rreiebPdtVDCT9eD_kb1PwIbIaX7opRS37-zT9LMiUAPGkKQpztDROGddprxAWtoCgXMFNJyhhQWgbEHlcjm4M2sUYiiW-PcJbArqiAKFV_G0uYK5jcYLmmDRr22axN0VINJOAbutiWWZwTVhoo1lLr94v2sVi4knv0MgRMbjJprHMvclJ6BBvNjbr0qk9Z7ug-slilCmhXaOrm_vxm9Wr-4v80LqVIS75ZiSCQ6zWyB6QTY8TA3FtPV9CssJy4IATsl6ptuvY4gMxwntD_V7am3EWRpMCfhj_d_Lg-IPteJPPgG_5kQw",
    "Content-Type": "application/json",
  },
  data: data,
};

axios(config)
  .then(function (response) {
    console.log(JSON.stringify(response.data));
  })
  .catch(function (error) {
    console.log(error);
  });
