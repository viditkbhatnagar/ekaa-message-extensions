const axios = require("axios");
// const url = "https://ticketappekaa.azurewebsites.net/";
const { ticketApplication } = require("../config/config");
const url = ticketApplication.baseUrl;

const getUserAllTickets = async (user_id) => {
  try {
    const resp = await axios.get(
      `https://ticketappekaa.azurewebsites.net/User/${user_id}/allTickets/`
    );
    if (resp && resp.data) {
      return {
        status: "Success",
        data: resp.data,
      };
    }
  } catch (err) {
    if (err.response) {
      return {
        status: "Failed",
        error: err.response.data,
      };
    }
  }
};

module.exports.checkTicket = async (user_id, ticket_id) => {
  let getTickets = await getUserAllTickets(user_id);
  let result = getTickets.data.data;

  let response = result.filter((tick) => {
    if (tick.ticket_id === ticket_id) {
      return tick;
    }
  });
  if (response) {
    return response[0];
  } else {
    return false;
  }
};

module.exports.getTicketHistory = async (user_id, ticket_id) => {
  try {
    let getTickets = await getUserAllTickets(user_id);
    let result = getTickets.data.data;
    let response = result.filter((tick) => {
      if (tick.ticket_id === ticket_id) {
        return tick;
      }
    });
    if (response) {
      return response[0].ticket_activity;
    } else {
      return false;
    }
  } catch (error) {
    console.log("error", error);
  }
};
