module.exports = {
  getUserTicket: require("./getUserAllTicketAPI").checkTicket,
  getTicketHistory: require("./getUserAllTicketAPI").getTicketHistory,
};
