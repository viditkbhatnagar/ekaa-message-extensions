const { TeamsActivityHandler, MessageFactory } = require("botbuilder");
const { CardFactory } = require("botbuilder");
const config = require("../config/config");
const { TaskModulesIds } = require("../models/taskModules/taskmoduleid");
const {
  TaskModulesSubmitIds,
} = require("../models/taskModules/taskmoduleSubmitID");
const {
  TaskModuleResponseFactory,
} = require("../models/taskModules/taskmoduleresponsefactory");
const {
  TaskModuleUIConstants,
} = require("../models/taskModules/taskmoduleuiconstants");
const {
  ticketDetailsCard,
  suggestionsCard,
  updateTicketCard,
  agentNotification,
  feedbackResolveCard,
  statusChangeNotification,
  commentNotification,
  messageExtensionTicketCard,
  welcomeCardAgent,
} = require("../utilities/cards");
const { saveUserFeedback } = require("../apiFunctions/saveUserFeedback");
const {
  sendProactiveMessages,
  saveManagerReference,
  getManagerReference,
  getCallerEmail,
} = require("../services/");
// const { userProfile,agentProfile } = require("../stateManager/");
const { UserProfile } = require("../stateManager/userProfile");
const { AgentProfile } = require("../stateManager/agentProfile");
const {
  raiseTicket,
  updateTicket,
  userAddComment,
  agentAddComment,
  updateTicketStatus,
} = require("../models/taskModules/taskModuleFunction/index");
const { getUserTicket } = require("../message_extension/");

class Bot extends TeamsActivityHandler {
  constructor(conversationState, dispatcher, userState) {
    super();

    if (!conversationState)
      throw new Error(
        "[Bot]: Missing parameter. conversationState is required"
      );
    if (!dispatcher)
      throw new Error("[Bot]: Missing parameter. dispatcher is required");

    this.conversationState = conversationState;
    this.userState = userState;
    this.dispatcher = dispatcher;
    this.baseUrl = config.ticket_App_Ui.baseUrl;
    this.dialogStateAccessor = conversationState.createProperty("DialogState");

    this.AgentProfileAccessor = userState.createProperty("AGENT");
    this.UserProfileAccessor = userState.createProperty("USER");

    this.onMessage(async (context, next) => {
      // console.log();
      // console.log('Calling User');
      // getUserPresence('fd39393d-5e20-4e63-93d8-b3aef545d062')
      // getMyUserPresence()
      console.log(new Date().toString());
      if (
        context.activity.value &&
        context.activity.value.action === "Feedback123"
      ) {
        // let saveFeedback = await saveUserFeedback(context.activity.value);
        await context.sendActivity("Thanks for your feedback");
      } else {
        console.log(context.activity.replyToId);
        await this.dispatcher.run(context, this.dialogStateAccessor);
      }
      await next();
    });
  }

  async handleTeamsTaskModuleFetch(context, taskModuleRequest) {
    try {
      let bot_id = process.env.MicrosoftAppId;

      let agentData = await this.AgentProfileAccessor.get(
        context,
        new AgentProfile()
      );
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("in bot userdata--------", userData);
      console.log("a----", taskModuleRequest);
      let cardTaskFetchValue = taskModuleRequest.data.data;
      console.log("i am slow------", cardTaskFetchValue);

      //const queryString = "?&appId=" + process.env.MicrosoftAppID + "&theme=" + String(taskModuleRequest.context.theme).toLowerCase() + "&token=" + JWTToken()

      let taskInfo = {};
      if (cardTaskFetchValue === TaskModulesIds.CHECK_TICKET_STATUS) {
        console.log("Check Ticket Status");

        taskInfo.url = taskInfo.fallbackUrl =
          this.baseUrl +
          "/" +
          `check_ticket_status?user_id=${userData.USER_ID}&bot_id=${bot_id}`;
        console.log(taskInfo.url);
        this.setTaskInfo(taskInfo, TaskModuleUIConstants.CHECK_TICKET_STATUS);
      } else if (cardTaskFetchValue === TaskModulesIds.RAISE_TICKET) {
        console.log("Raise Ticket");

        taskInfo.url = taskInfo.fallbackUrl =
          this.baseUrl +
          "/" +
          `raiseTicket?user_id=${userData.USER_ID}&bot_id=${bot_id}`;
        console.log(taskInfo.url);
        this.setTaskInfo(taskInfo, TaskModuleUIConstants.RAISE_TICKET);
      } else if (cardTaskFetchValue === TaskModulesIds.SHOW_TICKETS_TO_AGENT) {
        console.log("Show All Ticket to Agent");

        taskInfo.url = taskInfo.fallbackUrl =
          this.baseUrl +
          "/" +
          `getAgentTickets?agent_id=${agentData.AGENT_ID}&bot_id=${bot_id}`;
        console.log(taskInfo.url);
        this.setTaskInfo(taskInfo, TaskModuleUIConstants.SHOW_TICKETS_TO_AGENT);
      }
      return TaskModuleResponseFactory.toTaskModuleResponse(taskInfo);
    } catch (error) {
      console.log(error);
    }
  }

  async handleTeamsTaskModuleSubmit(context, taskModuleRequest) {
    let agentData = await this.AgentProfileAccessor.get(
      context,
      new AgentProfile()
    );
    console.log("inside submit handler-------------");
    console.log(taskModuleRequest);
    let data = taskModuleRequest.data;

    //Raise Ticket Submit Handler
    if (taskModuleRequest.data.taskId === TaskModulesSubmitIds.RAISE_TICKET) {
      context.activity.value.TASK_ID = "RAISE_TICKET";
      console.log("Raise Ticket Payload Data", data);
      let raiseUserTicket = await raiseTicket(data.payload, data.user_id);
      if (raiseUserTicket.status === "Success") {
        await context.sendActivity({
          attachments: [
            CardFactory.adaptiveCard(
              ticketDetailsCard(
                raiseUserTicket.data.assignment_details.ticket_id,
                raiseUserTicket.data.ticket_details.title,
                raiseUserTicket.data.ticket_details.department,
                raiseUserTicket.data.ticket_details.issue,
                raiseUserTicket.data.ticket_details.description,
                raiseUserTicket.data.ticket_details.issue_observed_on,
                raiseUserTicket.data.ticket_details.impact,
                raiseUserTicket.data.ticket_details.urgency,
                "1"
              )
            ),
          ],
        });
        await context.sendActivity({
          attachments: [CardFactory.adaptiveCard(suggestionsCard())],
        });

        console.log(
          "Agent Email",
          raiseUserTicket.data.assignment_details.agent_email
        );
        const fetchAgentManagerReference = await getManagerReference(
          raiseUserTicket.data.assignment_details.agent_email
        );

        if (fetchAgentManagerReference) {
          if (
            fetchAgentManagerReference.userId &&
            fetchAgentManagerReference.convId &&
            fetchAgentManagerReference.aadObjectId &&
            fetchAgentManagerReference.tenantID
          ) {
            let manager_reference = await sendProactiveMessages(
              fetchAgentManagerReference.userId,
              fetchAgentManagerReference.convId,
              fetchAgentManagerReference.aadObjectId,
              fetchAgentManagerReference.tenantID
            );
            console.log("Manager Reference", manager_reference);
            if (manager_reference) {
              const adapter = context.adapter;
              await adapter.continueConversationAsync(
                manager_reference,
                async (context) => {
                  let ticket_status = "New";
                  await context.sendActivity({
                    attachments: [
                      CardFactory.adaptiveCard(
                        agentNotification(
                          raiseUserTicket.data.assignment_details.agent_name,
                          raiseUserTicket.data.assignment_details.caller_name,
                          raiseUserTicket.data.assignment_details.ticket_id,
                          raiseUserTicket.data.ticket_details.title,
                          raiseUserTicket.data.ticket_details.description,
                          // raiseUserTicket.data.ticket_details.ticket_status
                          "New"
                        )
                      ),
                    ],
                  });
                }
              );
            } else {
              console.log("Something Went Wrong ! ");
            }
          } else {
            console.log("No Manager Reference found for the Agent");
          }
        } else {
          console.log(fetchAgentManagerReference.error);
        }
      } else {
        await context.sendActivity(raiseUserTicket.error);
      }

      // await context.sendActivity({
      //   attachments: [CardFactory.adaptiveCard(suggestionCard())],
      // });
    }
    //User Modify Ticket Submit Handler
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.MODIFY_TICKET
    ) {
      context.activity.value.TASK_ID = "MODIFY_TICKET";

      //Generate Payload
      let res = {
        urgency: data.payload.urgency,
      };

      //Calling the API's
      console.log("Modify Ticket Payload Data", data.payload.urgency);
      let modifyTicket = await updateTicket(res, data.user_id, data.ticketId);
      if (modifyTicket.status === "Success") {
        console.log("Modify Ticket Success", modifyTicket.data);
        await context.sendActivity({
          attachments: [
            CardFactory.adaptiveCard(
              updateTicketCard(
                modifyTicket.data.data.ticket_id,
                modifyTicket.data.data.title,
                modifyTicket.data.data.department,
                modifyTicket.data.data.issue,
                modifyTicket.data.data.description,
                modifyTicket.data.data.issue_observed_on,
                modifyTicket.data.data.impact,
                modifyTicket.data.data.urgency
              )
            ),
          ],
        });
        await context.sendActivity({
          attachments: [CardFactory.adaptiveCard(suggestionsCard())],
        });
      } else {
        await context.sendActivity(modifyTicket.error);
      }
    }
    //User Add Comment Submit Handleraget
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.USER_ADD_COMMENT
    ) {
      context.activity.value.TASK_ID = "USER_ADD_COMMENT";
      // let res = data.data;
      // console.log('User Add Comment Payload Data', data);
      //Calling the API's
      let addUserComment = await userAddComment(
        data.payload,
        data.user_id,
        data.ticket_id
      );
      if (addUserComment.status === "Success") {
        await context.sendActivity("Your Comment Has Been Added Successfully");
      } else {
        await context.sendActivity(addUserComment.error);
      }
    }
    //Agent Change Status Submit Handler
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.AGENT_CHANGE_STATUS
    ) {
      console.log("in change status .....agent", taskModuleRequest.data);

      console.log("data.payload", data);

      let changeTicketStatus = await updateTicketStatus(
        data.payload,
        data.agent_id,
        data.ticket_id
      );
      if (changeTicketStatus.status === "Success") {
        let ticket_id = changeTicketStatus.data.ticket_details.ticket_id;
        let callerEmail = await getCallerEmail(ticket_id);
        console.log("Caller Details", callerEmail);
        if (callerEmail.status == "Success") {
          let caller_email = callerEmail.email;
          console.log("Caller Email is : ", caller_email);
          const fetchCallerReference = await getManagerReference(caller_email);
          if (fetchCallerReference) {
            if (
              fetchCallerReference.userId &&
              fetchCallerReference.convId &&
              fetchCallerReference.aadObjectId &&
              fetchCallerReference.tenantID
            ) {
              let manager_reference = await sendProactiveMessages(
                fetchCallerReference.userId,
                fetchCallerReference.convId,
                fetchCallerReference.aadObjectId,
                fetchCallerReference.tenantID
              );
              console.log("Manager Reference", manager_reference);
              if (manager_reference) {
                const adapter = context.adapter;
                console.log(changeTicketStatus);
                await adapter.continueConversationAsync(
                  manager_reference,
                  async (context) => {
                    let notes;
                    console.log(
                      changeTicketStatus.data.ticket_details
                        .ticket_cancellation_notes
                    );
                    if (
                      changeTicketStatus.data.ticket_details.ticket_state ===
                      "Cancelled"
                    ) {
                      notes =
                        changeTicketStatus.data.ticket_details
                          .ticket_cancellation_notes;
                    } else if (
                      changeTicketStatus.data.ticket_details.ticket_state ===
                      "Resolved"
                    ) {
                      notes =
                        changeTicketStatus.data.ticket_details
                          .ticket_resolution_notes;
                    } else if (
                      changeTicketStatus.data.ticket_details.ticket_state ===
                      "On Hold"
                    ) {
                      notes =
                        changeTicketStatus.data.ticket_details
                          .ticket_on_hold_reason;
                    } else {
                      notes =
                        "Hi user, your ticket is in progress . Your issue will resolve soon.";
                    }
                    console.log("Notes", notes);
                    if (
                      changeTicketStatus.data.ticket_details.ticket_state !==
                      "Resolved"
                    ) {
                      await context.sendActivity({
                        attachments: [
                          CardFactory.adaptiveCard(
                            statusChangeNotification(
                              `${agentData.AGENT_F_NAME} ${agentData.AGENT_L_NAME}`,
                              changeTicketStatus.data.ticket_details.ticket_id,
                              changeTicketStatus.data.ticket_details
                                .ticket_title,
                              changeTicketStatus.data.ticket_details
                                .title_description,
                              changeTicketStatus.data.ticket_details
                                .ticket_state,
                              notes
                            )
                          ),
                        ],
                      });
                    } else {
                      await context.sendActivity({
                        attachments: [
                          CardFactory.adaptiveCard(
                            feedbackResolveCard(
                              `${agentData.AGENT_F_NAME} ${agentData.AGENT_L_NAME}`,
                              changeTicketStatus.data.ticket_details.ticket_id,
                              changeTicketStatus.data.ticket_details
                                .ticket_title,
                              changeTicketStatus.data.ticket_details
                                .title_description,
                              changeTicketStatus.data.ticket_details
                                .ticket_state,
                              notes
                            )
                          ),
                        ],
                      });
                    }
                  }
                );
                // await context.sendActivity('Your ticket status has been updated successfully');
              } else {
                console.log("Something Went Wrong ! ");
              }
            } else {
              console.log("No Manager Reference found for the Agent");
            }
          } else {
            console.log(fetchCallerReference.error);
          }
        } else {
        }
        await context.sendActivity("Ticket state is updated successfully");
      } else {
        await context.sendActivity(changeTicketStatus.error);
      }
    }
    //Agent Add Comment Submit Handler
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.AGENT_ADD_COMMENT
    ) {
      context.activity.value.TASK_ID = "AGENT_ADD_COMMENT";
      // let res = data.data;
      // console.log("Inside Agent Add Comment Submit Handler");
      // console.log(data);
      console.log("Agent Add Comment", data);

      let addAgentComment = await agentAddComment(
        data.payload,
        data.agent_id,
        data.ticketId
      );
      if (addAgentComment.status === "Success") {
        // await context.sendActivity("Your Comment Has Been Added Successfully");
        let ticket_id = addAgentComment.data.data.ticket_details.ticket_id;
        let callerEmail = await getCallerEmail(ticket_id);
        if (callerEmail.status == "Success") {
          // let caller_email = "anshuman.ranjan@celebaltech.com";
          const fetchCallerReference = await getManagerReference(
            callerEmail.email
          );
          if (fetchCallerReference) {
            if (
              fetchCallerReference.userId &&
              fetchCallerReference.convId &&
              fetchCallerReference.aadObjectId &&
              fetchCallerReference.tenantID
            ) {
              let manager_reference = await sendProactiveMessages(
                fetchCallerReference.userId,
                fetchCallerReference.convId,
                fetchCallerReference.aadObjectId,
                fetchCallerReference.tenantID
              );
              console.log("Manager Reference", manager_reference);
              if (manager_reference) {
                const adapter = context.adapter;
                await adapter.continueConversationAsync(
                  manager_reference,
                  async (context) => {
                    await context.sendActivity({
                      attachments: [
                        CardFactory.adaptiveCard(
                          commentNotification(
                            `${agentData.AGENT_F_NAME} ${agentData.AGENT_L_NAME}`,
                            addAgentComment.data.data.ticket_details.ticket_id,
                            addAgentComment.data.data.ticket_details
                              .ticket_title,
                            addAgentComment.data.data.ticket_details
                              .ticket_descritpion,
                            addAgentComment.data.data.comment
                          )
                        ),
                      ],
                    });
                  }
                );
              } else {
                console.log("Something Went Wrong ! ");
              }
            } else {
              console.log("No Manager Reference found for the Agent");
            }
          } else {
            console.log(fetchCallerReference.error);
          }
        } else {
          console.log("No Caller Email Found");
        }

        await context.sendActivity("Your Comment Has Been Added Successfully");
      }
    } else {
      await context.sendActivity(addAgentComment.error);
    }
  }

  setTaskInfo(taskInfo, uiSettings) {
    taskInfo.height = uiSettings.height;
    taskInfo.width = uiSettings.width;
    taskInfo.title = uiSettings.title;
  }

  async run(context) {
    await super.run(context);
    await this.conversationState.saveChanges(context, false);
    await this.userState.saveChanges(context, false);
  }

  async handleTeamsMessagingExtensionFetchTask(context, action) {
    // console.log(context.activity.value.messagePayload, "VVVVVVVVVVVVVVVVVV");
    // console.log(context.activity.value.messagePayload.prompt, "VVVVVVVVVVVVVVVVVV");
    // console.log(action.messagePayload.body, "fffffffffffffffffffffffffffffff");
    if (action.commandId == "FetchRoster") {
      let bot_id = process.env.MicrosoftAppId;
      console.log("Inside Fetch Roster");
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("User Data", userData.USER_ID);
      console.log(bot_id);
      return {
        task: {
          type: "continue",
          value: {
            width: 680,
            height: 530,
            title: "Raise Ticket Module",
            url: `https://dev-ekaaui.azurewebsites.net/messageExtension/raiseTicket?user_id=USR2&bot_id=${bot_id}`,
          },
        },
      };
    } else if (action.commandId == "Static HTML") {
      let bot_id = process.env.MicrosoftAppId;
      console.log("Inside Static HTML");
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("User Data", userData.USER_ID);
      console.log(bot_id);
      return {
        task: {
          type: "continue",
          value: {
            width: 680,
            height: 530,
            title: "Check Ticket Status Module",
            url: `https://dev-ekaaui.azurewebsites.net/messageExtension/check_ticket_status?user_id=USR2&bot_id=${bot_id}`,
          },
        },
      };
    } else if (action.commandId == "shareMessage") {
      let bot_id = process.env.MicrosoftAppId;
      console.log("Inside Raise Ticket Module Extension");
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("User Data", userData.USER_ID);
      console.log(bot_id);

      function removeTags(str) {
        if (str === null || str === "") return false;
        else str = str.toString();
        return str.replace(/(<([^>]+)>)/gi, "");
      }

      let message = await removeTags(action.messagePayload.body.content);
      console.log(message);
      return {
        task: {
          type: "continue",
          value: {
            width: 680,
            height: 530,
            title: "Raise Ticket Module",
            url: `https://dev-ekaaui.azurewebsites.net/messageExtension/raiseTicket?user_id=USR2&bot_id=${bot_id}&titleurl=${message}`,
          },
        },
      };
    } else if (action.commandId == "shareMessages") {
      let bot_id = process.env.MicrosoftAppId;
      console.log("Inside get ticket status Extension");
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("User Data", userData.USER_ID);
      console.log(bot_id);
      return {
        task: {
          type: "continue",
          value: {
            width: 680,
            height: 530,
            title: "Ticket Status Module",
            url: `https://dev-ekaaui.azurewebsites.net/messageExtension/check_ticket_status?user_id=USR2&bot_id=${bot_id}`,
          },
        },
      };
    }
  }

  async handleTeamsMessagingExtensionQuery(context, query) {
    const searchQuery = query.parameters[0].value;
    const attachments = [];
    let response;
    if (
      searchQuery.slice(0, 4).trim().toLowerCase() == "tick" &&
      searchQuery.length >= 5
    ) {
      console.log("Inside Message Extension");
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("User Data", userData.USER_ID);
      const response = await getUserTicket(
        userData.USER_ID,
        searchQuery.toUpperCase()
      );
      if (response) {
        console.log(`These are Your Ticket Details
        Ticket Id : ${response.ticket_id}
        Ticket Title : ${response.title}
        Ticket Department : ${response.department}
        Ticket Description : ${response.description}
        Ticket Caller : ${response.caller}
        `);
      } else {
        console.log("No tickets exist");
      }
      const preview = CardFactory.thumbnailCard(
        `Ticket ID : ${response.ticket_id}`,
        "Click here to view ticket details"
      );

      const adaptive = CardFactory.adaptiveCard(
        messageExtensionTicketCard(
          response.ticket_id,
          response.title,
          response.department,
          response.issue,
          response.description,
          response.issue_observed_on,
          response.impact,
          response.urgency,
          "2",
          response.ticket_activity
        )
      );
      // const adaptive = CardFactory.adaptiveCard(welcomeCardAgent());
      const attachment = { ...adaptive, preview };

      return {
        composeExtension: {
          type: "result",
          attachmentLayout: "list",
          attachments: [attachment],
        },
      };
    } else {
      console.log("VVVVVVVVVVVVVVvvv");
      const preview = CardFactory.thumbnailCard(
        `Ticket ID : ${response.ticket_id}`,
        "Click here to view ticket details"
      );
    }
  }

  async handleTeamsMessagingExtensionSelectItem(context, obj) {
    return {
      composeExtension: {
        type: "result",
        attachmentLayout: "list",
        attachments: [CardFactory.thumbnailCard(obj.description)],
      },
    };
  }

  async handleTeamsSigninVerifyState(context, state) {
    await this.dispatcher.run(context, this.dialogStateAccessor);
  }
}

module.exports.Bot = Bot;
