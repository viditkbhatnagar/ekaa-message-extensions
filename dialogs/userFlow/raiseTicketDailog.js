const {
  ComponentDialog,
  WaterfallDialog,
  ChoiceFactory,
} = require("botbuilder-dialogs");
const { ActivityTypes,ActionTypes,MessageFactory} = require("botbuilder");
const { DailogConst, ButtonConst } = require("../../utilities/constants");
const { LuisRecognizer } = require("botbuilder-ai");
const { CardFactory } = require("botbuilder");
const { luisConfiguration } = require("../../config/config");
const { UserProfile } = require("../../stateManager/userProfile");
const {urgencyCard,updatedUrgencyCard,impactCard,updateImpactCard,choiceCard,updatedChoiceCard,ticketDetailsCard,suggestionsCard} = require("../../utilities/cards/");
const Recognizers = require("@microsoft/recognizers-text-suite");
const luisConfig = {
  applicationId: luisConfiguration.LuisAppId,
  endpointKey: luisConfiguration.LuisEndpointKey,
  endpoint: luisConfiguration.LuisEndpoint,
};
const { raiseUserTicket } = require("../../apiFunctions/raiseTicket");
const {AskQuestion} = require('./askQuestion');
let dialogState;
let entities;
let luisIntent;
let newActivity;
let updatedCard;
class RaiseTicketDialog extends ComponentDialog {
  constructor(conversationState,userState) {
    super(DailogConst.RAISE_TICKET_DIALOG);
    if(!conversationState) throw new Error("[conversationState] cannot be null");
    if(!userState) throw new Error("[userState] cannot be null");
    this.conversationState = conversationState;
    this.recognizer = new LuisRecognizer(luisConfig, {
      apiVersion: "v3",
    });
    this.raiseTicketData =
      this.conversationState.createProperty("RAISE_A_TICKET");
      this.UserProfileAccessor = userState.createProperty("USER");
      this.addDialog(new AskQuestion(conversationState,userState));
    this.addDialog(
      new WaterfallDialog(DailogConst.RAISE_TICKET_DIALOG_WF, [
        this.raiseTicketStep.bind(this),
        this.getTicketTitle.bind(this),
        this.getTicketDescription.bind(this),
        this.getIssue.bind(this),
        this.getTicketDate.bind(this),
        this.getUrgency.bind(this),
        this.getImpact.bind(this),
        this.getTicketAttachment.bind(this),
        this.raiseTicketFinal.bind(this),
        this.handleSuggestion.bind(this),
      ])
    );

    this.initialDialogId = DailogConst.RAISE_TICKET_DIALOG_WF;
  }

  async raiseTicketStep(stepContext) {
    console.log("raiseTicketStep");
    await stepContext.context.sendActivities([
      { type: ActivityTypes.Typing },
      { type: "delay", value: 1500 },
      {
        type: ActivityTypes.Message,
        text: "Hello Shuvam, I am a EKAA Digital Assistant Bot. I will help you raise a ticket.",
      },
    ]);
    await stepContext.context.sendActivities([
      { type: ActivityTypes.Typing },
      { type: "delay", value: 1500 },
      {
        type: ActivityTypes.Message,
        text: "Can You Please tell me the problem you are facing in brief.",
      },
    ]);
    return ComponentDialog.EndOfTurn;
  }

  async getTicketTitle(stepContext) {
    dialogState = await this.raiseTicketData.get(stepContext.context, {});
    dialogState.ticketTitle = stepContext.context.activity.text;
    await stepContext.context.sendActivities([
      { type: ActivityTypes.Typing },
      { type: "delay", value: 1500 },
      {
        type: ActivityTypes.Message,
        text: "Ok, Can you now please elaborate the issue in details. It will help us to analyze the issue in a better way.",
      },
    ]);

    return ComponentDialog.EndOfTurn;
  }

  async getTicketDescription(stepContext) {
    dialogState.ticketDescription = stepContext.context.activity.text;
    let luisresponse = await this.recognizer.recognize(dialogState.ticketTitle);
    luisIntent = luisresponse.luisResult.prediction.topIntent;
    entities = luisresponse.luisResult.prediction.entities.Issues[0];
    console.log("Department is :", luisIntent);
    console.log("Issue is :", entities);
    switch (luisIntent.toLowerCase()) {
      case "hr":
        await stepContext.context.sendActivities([
          { type: ActivityTypes.Typing },
          { type: "delay", value: 1500 },
          {
            type: ActivityTypes.Message,
            text: `After analyzing the issue, I think you are facing ${entities} related issue.`,
          },
        ]);
        break;
      case "logistics":
        await stepContext.context.sendActivities([
          { type: ActivityTypes.Typing },
          { type: "delay", value: 1500 },
          {
            type: ActivityTypes.Message,
            text: `After analyzing the issue, I think you are facing ${entities} related issue.`,
          },
        ]);
        break;
    }
    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          choiceCard(`Do you want me to raise a Ticket to the ${entities} Issue?`)
        ),
      ],
    });
    return ComponentDialog.EndOfTurn;
  }

  async getIssue(stepContext) {
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Choices" && stepContext.context.activity.value.value == "Yes") {
      dialogState.ticketIssue = entities;
      updatedCard = CardFactory.adaptiveCard(
        updatedChoiceCard(`Do you want me to raise a Ticket to the ${entities} Issue?`)
      ),
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);
      dialogState.ticketDepartment = luisIntent;
      await stepContext.context.sendActivities([
        { type: ActivityTypes.Typing },
        { type: "delay", value: 1500 },
        {
          type: ActivityTypes.Message,
          text: `Can you please tell me from which date you are facing the issue`,
        },
      ]);
    } else {
      console.log("No");
      return stepContext.replaceDialog(DailogConst.RAISE_TICKET_DIALOG);
    }

    return ComponentDialog.EndOfTurn;
  }

  async getTicketDate(stepContext) {
    dialogState.ticketDate = stepContext.result;
    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          urgencyCard()
        ),
      ],
    });

    return ComponentDialog.EndOfTurn;  
  }

  async getUrgency(stepContext) {
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Urgency") {
      dialogState.ticketUrgency = stepContext.context.activity.value.value;
      updatedCard = CardFactory.adaptiveCard(
        updatedUrgencyCard()
      ),
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);
    } else {
      dialogState.ticketUrgency = "Low";
    }
    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          impactCard()
        ),
      ],
    });
    return ComponentDialog.EndOfTurn;
  }

  async getImpact(stepContext) {
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Impact") {
      updatedCard = CardFactory.adaptiveCard(
        updateImpactCard()
      ),
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);
    } else {
      dialogState.ticketImpact = "Impacts Me";
    }
    // await stepContext.prompt(
    //   ConfirmPromptDialog,
    //   `Do you want to upload any attachment supporting your issue?`,
    //   ["Yes", "No"]
    // );
    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          choiceCard(`Do you want to upload any attachment supporting your issue?`)
        ),
      ],
    });
    return ComponentDialog.EndOfTurn;
  }

  async getTicketAttachment(stepContext) {
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Choices" && stepContext.context.activity.value.value == "Yes") {
      dialogState.ticketAttachment = stepContext.context.activity.text;
    } else {
      dialogState.ticketAttachment = "";
    }
    updatedCard = CardFactory.adaptiveCard(
      updatedChoiceCard(`Do you want to upload any attachment supporting your issue?`)
    ),
    updatedCard.id = stepContext.context.activity.replyToId;
    newActivity =  MessageFactory.attachment(updatedCard);
    newActivity.id = stepContext.context.activity.replyToId;
    await stepContext.context.updateActivity(newActivity);
    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          choiceCard(`After analyzing all the points, I think you are facing ${luisIntent} related issue. Do You want me to raise a ticket to the ${luisIntent} team?`)
        ),
      ],
    });
    return ComponentDialog.EndOfTurn;
  }

  async raiseTicketFinal(stepContext) {
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Choices" && stepContext.context.activity.value.value == "Yes") {
      updatedCard = CardFactory.adaptiveCard(
        updatedChoiceCard(`Do you want to upload any attachment supporting your issue?`)
      ),
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);
      let ticket = await raiseUserTicket(dialogState);
      if (ticket.status === "Success") {
        let res = ticket.data;
        console.log(res);
        await stepContext.context.sendActivity({
          attachments: [
            CardFactory.adaptiveCard(
              ticketDetailsCard(
                res.assignment_details.ticket_id,
                res.ticket_details.title,
                res.ticket_details.department,
                res.ticket_details.issue,
                res.ticket_details.description,
                res.ticket_details.issue_observed_on,
                res.ticket_details.impact,
                res.ticket_details.urgency,
                "1"
              )
            ),
          ],
        });
        await stepContext.context.sendActivity({
          attachments: [CardFactory.adaptiveCard(suggestionsCard())],
        });
      } else {
        console.log(ticket.error);
      }
    } else {
      await stepContext.context.sendActivities([
        { type: ActivityTypes.Typing },
        { type: "delay", value: 1500 },
        {
          type: ActivityTypes.Message,
          text: `Thank you for your time. Do connect if you want to raise a ticket.`,
        },
      ]);
      return stepContext.replaceDialog(DailogConst.RAISE_TICKET_DIALOG);
    }
    return ComponentDialog.EndOfTurn;
  }

  async handleSuggestion(stepContext) {
    if(stepContext.context.activity.value && stepContext.context.activity.value.action === "Ask Question"){
      return stepContext.beginDialog(DailogConst.ASK_A_QUESTION_DIALOG);
    }else{
      return stepContext.endDialog();
    }
  }
  
  
}

module.exports.RaiseTicketDialog = RaiseTicketDialog;
