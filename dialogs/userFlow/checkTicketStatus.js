const {
  ComponentDialog,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { DailogConst, ButtonConst } = require("../../utilities/constants");
const { CardFactory,ActivityTypes,ActionTypes,MessageFactory } = require("botbuilder");
const {getUserDetails,fetchTicketStatusOfUser} = require("../../apiFunctions/");
const {choiceCard,updatedChoiceCard} = require("../../utilities/cards/");
const { UserProfile } = require("../../stateManager/userProfile");
const { TeamsInfo } = require("botbuilder");
let ticketState;
let status;
let updatedCard;
let newActivity;
class CheckTicketStatus extends ComponentDialog {
  constructor(conversationState,userState) {
    super(DailogConst.CHECK_TICKET_STATUS_DIALOG);
    if(!conversationState){
      throw new Error("[CheckTicketStatus]: Missing parameter. conversationState is required");
    }
    if(!userState){
      throw new Error("[CheckTicketStatus]: Missing parameter. userState is required");
    }
    this.conversationState = conversationState;
    this.checkTicketState =
      this.conversationState.createProperty("CHECK_TICKET_STATE");
      this.UserProfileAccessor = userState.createProperty("USER");
    this.addDialog(
      new WaterfallDialog(DailogConst.CHECK_TICKET_STATUS_DIALOG_WF, [
        this.checkTicketStep.bind(this),
        this.checkTicketStep2.bind(this),
        this.checkTicketStep3.bind(this),
      ])
    );
    this.initialDialogId = DailogConst.CHECK_TICKET_STATUS_DIALOG_WF;
  }

  async checkTicketStep(stepContext) {
    ticketState = await this.checkTicketState.get(stepContext.context, {});
    if (stepContext.options.entities && stepContext.options.luisResult) {
      return await stepContext.next();
    } else {
      await stepContext.context.sendActivity("Please Enter the Ticket Number");
      return ComponentDialog.EndOfTurn;
    }
  }

  async checkTicketStep2(stepContext) {
    if (stepContext.options.entities) {
      ticketState.ticketNumber = stepContext.options.entities[0];
    } else {
      ticketState.ticketNumber = stepContext.result;
    }
    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          choiceCard(`Do you also want to see the details of the ticket **${ticketState.ticketNumber}**?`)
        ),
      ],
    });
    return ComponentDialog.EndOfTurn;
  }

  async checkTicketStep3(stepContext) {
    updatedCard = CardFactory.adaptiveCard(
      updatedChoiceCard(`Do you also want to see the details of the ticket **${ticketState.ticketNumber}**?`)
    );
    let userData = await this.UserProfileAccessor.get(
      stepContext.context,
      new UserProfile()
    );
      console.log('Email is :',userData.email);
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);

      // const member = await TeamsInfo.getMember(
      //   stepContext.context,
      //   stepContext.context.activity.from.id
      // );
      // let email = member.email;
      let user_details = await getUserDetails(userData.USER_EMAIL);
      if(user_details.status === "Success"){
        ticketState.user_id = user_details.data.data.user_id;
      }
      await stepContext.context.sendActivity(`Fetching the status of the ticket **${ticketState.ticketNumber}**`);
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Choices" && stepContext.context.activity.value.value == "Yes") {
      await stepContext.context.sendActivity(
        `Fecthing Details & status of ${ticketState.ticketNumber} of ${ticketState.user_id}`
      );
    } else {
      console.log(
        ticketState.user_id,
        ticketState.ticketNumber
      )
      let status = await fetchTicketStatusOfUser(ticketState.user_id,ticketState.ticketNumber);
      if(status.status === "Success"){
        ticketState.ticketStatus = status.data;
        await stepContext.context.sendActivity(
          `Ticket Status of **${ticketState.ticketNumber}** is **${ticketState.ticketStatus}**`
        );
      }else{
        await stepContext.context.sendActivity(
          `No Ticket Exist with this id`
        );
      }
    }
    return stepContext.endDialog();
  }
}

module.exports.CheckTicketStatus = CheckTicketStatus;
