const {
  WaterfallDialog,
  ComponentDialog,
} = require("botbuilder-dialogs");
// const { ActivityTypes } = require("botbuilder");
const { ActivityTypes,ActionTypes,MessageFactory} = require("botbuilder");
const { QnAMaker } = require("botbuilder-ai");
const { raiseQueryCard,sorryCard,suggestionsCard,choiceCard,updatedChoiceCard} = require("../../utilities/cards/");
const config = require("../../config/config");
const { CardFactory } = require("botbuilder");
const { DailogConst } = require("../../utilities/constants");
const { CancelAndHelpDialog } = require("./interruptDialog");
const { FeedbackDialog } = require("./feedback");
let updatedCard;
let newActivity;
let message;

class AskQuestion extends CancelAndHelpDialog {
  constructor(conversationState,userState) {
    super(DailogConst.ASK_A_QUESTION_DIALOG);

    if(!conversationState){
      throw new Error("[AskQuestionDialog]: Missing conversation state");
    }
    if(!userState){
      throw new Error("[AskQuestionDialog]: Missing user state");
    }

    this.qnaMaker = new QnAMaker({
      knowledgeBaseId: config.qnamaker.QnAKnowledgebaseId,
      endpointKey: config.qnamaker.QnAEndpointKey,
      host: config.qnamaker.QnAEndpointHostName,
    });
    this.addDialog(new FeedbackDialog(conversationState,userState));

    this.addDialog(
      new WaterfallDialog(DailogConst.ASK_A_QUESTION_DIALOG_WF, [
        this.askToEnterQuestion.bind(this),
        this.QueryStep.bind(this),
        this.FeedbackSwitch.bind(this),
      ])
    );

    this.initialDialogId = DailogConst.ASK_A_QUESTION_DIALOG_WF;
  }

  async askToEnterQuestion(stepContext) {
    // await stepContext.context.sendActivity("Please Enter your Query below");
    await stepContext.context.sendActivities([
      { type: ActivityTypes.Typing },
      { type: "delay", value: 3000 },
      {
        type: ActivityTypes.Message,
        text: "Hey, I can help you solve your query. Can you please type your query below.",
      },
    ]);
    return ComponentDialog.EndOfTurn;
  }

  async QueryStep(stepContext) {
    console.log(stepContext.context.activity.text);
    const qnaResults = await this.qnaMaker.getAnswers(stepContext.context);
    let question = stepContext.context.activity.text;
    console.log(qnaResults);
    if (qnaResults[0]) {
      // await stepContext.context.sendActivity(qnaResults[0].answer);
      let answer = qnaResults[0].answer;
      await stepContext.context.sendActivity({
        attachments: [
          CardFactory.adaptiveCard(raiseQueryCard(question, answer)),
        ],
      });
    } else {
      await stepContext.context.sendActivity({
        attachments: [CardFactory.adaptiveCard(sorryCard(question))],
      });

      await stepContext.context.sendActivity({
        attachments: [CardFactory.adaptiveCard(suggestionsCard())],
      });
    }


    await stepContext.context.sendActivity({
      attachments: [
        CardFactory.adaptiveCard(
          choiceCard(`Do you want to give us your valuable feedback?`)
        ),
      ],
    });

    return ComponentDialog.EndOfTurn;
  }

  async FeedbackSwitch(stepContext) {
    if (stepContext.context.activity.value && stepContext.context.activity.value.action == "Choices" && stepContext.context.activity.value.value == "Yes") {
      updatedCard = CardFactory.adaptiveCard(
        updatedChoiceCard(`Do you want to give us your valuable feedback?`)
      ),
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);
      return await stepContext.beginDialog(DailogConst.USER_FEEDBACK_DIALOG);
    } else {
      updatedCard = CardFactory.adaptiveCard(
        updatedChoiceCard(`Do you want to give us your valuable feedback?`)
      ),
      updatedCard.id = stepContext.context.activity.replyToId;
      newActivity =  MessageFactory.attachment(updatedCard);
      newActivity.id = stepContext.context.activity.replyToId;
      await stepContext.context.updateActivity(newActivity);
      message = "Thank You for your time";
      await stepContext.context.sendActivity(message);
      await stepContext.context.sendActivity({
        attachments: [CardFactory.adaptiveCard(suggestionsCard())],
      });
      return await stepContext.endDialog();
    }
  }
}

module.exports.AskQuestion = AskQuestion;
