const {
  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { CardFactory, TeamsInfo, MessageFactory } = require("botbuilder");
const { loginCard } = require("../../utilities/cards/");
const { LuisRecognizer } = require("botbuilder-ai");
const { UserDialog } = require("../../dialogs/userFlow/mainUserDialog");
const { AgentMainDialog } = require("./mainAgentDialog");
// const {AgentMainD} = require("./");
const { DailogConst } = require("../../utilities/constants");
const { luisConfiguration } = require("../../config/config");
const {RaiseTicket,CheckTicketStatus} = require("../userFlow/index");
const luisConfig = {
  applicationId: luisConfiguration.LuisAppId,
  endpointKey: luisConfiguration.LuisEndpointKey,
  endpoint: luisConfiguration.LuisEndpoint,
};
const AgentDispatcherWaterfall = "AgentDispatcherWaterfall";

class AgentDispatcher extends ComponentDialog {
  constructor(conversationState,userState) {
    super(DailogConst.AGENT_DISPATCHER_DIALOG);

    if (!conversationState) {
      throw new Error(
        "[AgentDispatcher]: Missing parameter. conversationState is required"
      );
    }

    if(!userState){
      throw new Error("[AgentDispatcher]: Missing parameter. userState is required");
    }


    this.recognizer = new LuisRecognizer(luisConfig, {
      apiVersion: "v3",
    });

    this.addDialog(new RaiseTicket(conversationState,userState));
    this.addDialog(new UserDialog(conversationState,userState));
    this.addDialog(new AgentMainDialog(conversationState,userState));
    this.addDialog(new CheckTicketStatus(conversationState,userState));

    this.addDialog(
      new WaterfallDialog(DailogConst.AGENT_DISPATCHER_DIALOG_WF, [
        this.PersonaCard.bind(this),
        this.switchPersona.bind(this),
      ])
    );

    this.initialDialogId = DailogConst.AGENT_DISPATCHER_DIALOG_WF;
  }

  async PersonaCard(stepContext) {
    try {
      let card = MessageFactory.attachment(
        CardFactory.adaptiveCard(loginCard())
      );
      await stepContext.context.sendActivity(card);

      return ComponentDialog.EndOfTurn;
    } catch (err) {
      console.log(err);
    }
  }
  async switchPersona(stepContext) {
    if (stepContext.context.activity.value) {
      if (stepContext.context.activity.value.action == "USER") {
        // await stepContext.cancelAllDialogs();
        return await stepContext.beginDialog(DailogConst.USER_MAIN_DIALOG);
      } else if (stepContext.context.activity.value.action == "AGENT") {
        return await stepContext.beginDialog(DailogConst.AGENT_MAIN_DIALOG);
      }
    } else {
      let luisresponse = await this.recognizer.recognize(stepContext.context);
      let luisIntent = luisresponse.luisResult.prediction.topIntent;
      console.log(luisIntent);
      switch (luisIntent.toLowerCase()) {
        case "raiseticket":
          return await stepContext.beginDialog(DailogConst.RAISE_TICKET_DIALOG);
          break;
        case "checkticketstatus":
          console.log("here");
          console.log(luisresponse.luisResult.prediction);
          return await stepContext.beginDialog(
            DailogConst.CHECK_TICKET_STATUS_DIALOG,
            {
              luisResult: true,
              entities: luisresponse.luisResult.prediction.entities.ticket,
            }
          );
          break;
        default:
          return await stepContext.beginDialog(DailogConst.AGENT_DISPATCHER_DIALOG);
      }
    }
  }
}

module.exports.AgentDispatcher = AgentDispatcher;
