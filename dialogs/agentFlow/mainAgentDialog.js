const { WaterfallDialog, ComponentDialog } = require("botbuilder-dialogs");
const { welcomeCardAgent } = require("../../utilities/cards/");
const { CardFactory } = require("botbuilder");
const { luisConfig,cronTimer } = require("../../config/config");
const { DailogConst, ButtonConst } = require("../../utilities/constants");
// const { MainUserDialog } = require("./../userFlow/index");
const { UserDialog } = require("./../userFlow/mainUserDialog");
const CronJob = require("cron").CronJob;
const { MicroAnalyticsDialog } = require("../../dialogs/agentFlow/microAnalyticsDialog");



class AgentMainDialog extends ComponentDialog {
  constructor(conversationState,userState) {
    super(DailogConst.AGENT_MAIN_DIALOG);

    if(!conversationState){
      throw new Error("[AgentMainDialog]: Missing parameter. conversationState is required");
    }
    if(!userState){
      throw new Error("[AgentMainDialog]: Missing parameter. userState is required");
    }
    this.conversationState = conversationState;

    this.addDialog(new MicroAnalyticsDialog(conversationState));
    this.addDialog(new UserDialog(conversationState, userState));
    this.addDialog(
      new WaterfallDialog(DailogConst.AGENT_MAIN_DIALOG_WF, [
        this.agentStep1.bind(this),
        this.navigationStep.bind(this),
      ])
    );

    this.initialDialogId = DailogConst.AGENT_MAIN_DIALOG_WF;
  }

  async agentStep1(stepContext) {
    await stepContext.context.sendActivity({
      attachments: [CardFactory.adaptiveCard(welcomeCardAgent())],
    });
  console.log('Starting Cron Job Now');

  // new CronJob(
  //   cronTimer.cronTime,async() => {
  //     console.log('Cron Job Called');
  //     await stepContext.beginDialog(DailogConst.AGENT_MICRO_ANALYTICS_DIALOG);
  //   },
  //   null,
  //   true,
  //   "Asia/Kolkata"
  // );

  return ComponentDialog.EndOfTurn;
  }

async navigationStep(stepContext) {
  if(stepContext.context.activity && ((/^#user$/i).test(stepContext.context.activity.text))){
    return stepContext.beginDialog(DailogConst.USER_MAIN_DIALOG);
  }
  else if(stepContext.context.activity && ((/^hi$/i).test(stepContext.context.activity.text))){
    return stepContext.beginDialog(DailogConst.DISPATCHER_DIALOG);
  }
  else{
    return stepContext.endDialog(DailogConst.AGENT_MAIN_DIALOG);
  }
}


}

module.exports.AgentMainDialog = AgentMainDialog;
