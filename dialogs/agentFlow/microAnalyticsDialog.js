const { ComponentDialog, WaterfallDialog} = require('botbuilder-dialogs');
const { DailogConst, ButtonConst } = require("../../utilities/constants");
const CronJob = require("cron").CronJob;
const { sendProactiveMessages,getManagerReference,getCallerEmail } = require("../../services/");
const cron = require("node-cron");
const { Analytics } = require("../../cron/getAgentAnalytics");
const {agentMicroAnalytics} = require("../../utilities/cards/");
let inprogress;
let closed;
let onhold;
let newticket;

class MicroAnalyticsDialog extends ComponentDialog{
    constructor(conversationState){
        super(DailogConst.AGENT_MICRO_ANALYTICS_DIALOG);
        if(!conversationState){
            throw new Error("[MicroAnalyticsDialog]: Missing parameter. conversationState is required");
        }

        this.addDialog(new WaterfallDialog
            (DailogConst.AGENT_MICRO_ANALYTICS_DIALOG_WF, 
                [
                    this.analyticsStep1.bind(this)
                ]));
    }
    async analyticsStep1(stepContext){
        await stepContext.context.sendActivity("Inside Analytics Step 1");
         //   console.log('inside cron job',context);
            // console.log(context);
            const data = await Analytics(agentData.AGENT_ID);
            if (data.status == "Success") {
              console.log("here");
              inprogress = data.inprogress_ticket;
              closed = data.onhold_ticket;
              onhold = data.closed_ticket;
              newticket = data.new_ticket;
    
              console.log(agentData.AGENT_EMAIL);
              const fetchAgentManagerReference = await getManagerReference(
                agentData.AGENT_EMAIL
              );
              if (fetchAgentManagerReference) {
                if (
                  fetchAgentManagerReference.userId &&
                  fetchAgentManagerReference.convId &&
                  fetchAgentManagerReference.aadObjectId &&
                  fetchAgentManagerReference.tenantID
                ) {
                  let manager_reference = await sendProactiveMessages(
                    fetchAgentManagerReference.userId,
                    fetchAgentManagerReference.convId,
                    fetchAgentManagerReference.aadObjectId,
                    fetchAgentManagerReference.tenantID
                  );
                    console.log(manager_reference);
                  if (manager_reference) {
                    const adapter = context.adapter;
                    await adapter.continueConversation(
                      manager_reference,
                      async () => {
                        let ticket_status = "New";
                        await context.sendActivity({
                          attachments: [
                            CardFactory.adaptiveCard(
                              agentMicroAnalytics(
                                inprogress,
                                closed,
                                onhold,
                                newticket,
                              )
                            ),
                          ],
                        });
                      }
                    );
                  } else {
                    console.log("Something Went Wrong ! ");
                  }
                } else {
                  console.log("No Manager Reference found for the Agent");
                }
              } else {
                console.log("Error");
              }
            }
        return await stepContext.endDialog();
    }

}

module.exports.MicroAnalyticsDialog = MicroAnalyticsDialog;