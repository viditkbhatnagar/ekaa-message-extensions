require("isomorphic-fetch");
const axios = require("axios");
const { Client } = require("@microsoft/microsoft-graph-client");
const {
  TokenCredentialAuthenticationProvider,
} = require("@microsoft/microsoft-graph-client/authProviders/azureTokenCredentials");
const { ClientSecretCredential } = require("@azure/identity");

// const {microsoftGraphAPI} = require('../config/config');
// const { getMyUserPresence } = require('./presence/userPresence');

const credential = new ClientSecretCredential(
  "c882bcf1-a8c7-4295-86ed-416897035ffd",
  "347015e8-641f-451b-bc61-35e646a0d91d",
  "xWs7Q~Ks3NuO6qn9vVsVyQ6JXaWBQBh-zQ9jV"
);
const authProvider = new TokenCredentialAuthenticationProvider(credential, {
  scopes: " https://graph.microsoft.com/.default",
});

const client = Client.initWithMiddleware({
  debugLogging: true,
  authProvider,
  // Use the authProvider object to create the class.
});

const createChat = async function () {
  try {
    const chat = {
      chatType: "group",
      topic: "Chat API Testing",
      members: [
        {
          "@odata.type": "#microsoft.graph.aadUserConversationMember",
          roles: ["owner"],
          "user@odata.bind":
            "https://graph.microsoft.com/v1.0/users('016e04b2-078d-4d84-80e5-1a59be585afd')",
        },
        {
          "@odata.type": "#microsoft.graph.aadUserConversationMember",
          roles: ["owner"],
          "user@odata.bind":
            "https://graph.microsoft.com/v1.0/users('6f4658c6-b364-47ba-b75a-e9108b7cfca0')",
        },
        {
          "@odata.type": "#microsoft.graph.aadUserConversationMember",
          roles: ["owner"],
          "user@odata.bind":
            "https://graph.microsoft.com/v1.0/users('fd39393d-5e20-4e63-93d8-b3aef545d062')",
        },
      ],
    };
    var config = {
      method: "post",
      url: "https://graph.microsoft.com/v1.0/chats",
      headers: {
        Authorization:
          "Bearer eyJ0eXAiOiJKV1QiLCJub25jZSI6IkRkSUU3dW5vNzA3RXA1SEhpSVpxa1I3MDV1N1RSbVZHS2ZLY0E3WXg3N3MiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik1yNS1BVWliZkJpaTdOZDFqQmViYXhib1hXMCIsImtpZCI6Ik1yNS1BVWliZkJpaTdOZDFqQmViYXhib1hXMCJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9jODgyYmNmMS1hOGM3LTQyOTUtODZlZC00MTY4OTcwMzVmZmQvIiwiaWF0IjoxNjQyNjYwNjUwLCJuYmYiOjE2NDI2NjA2NTAsImV4cCI6MTY0MjY2NTAxNywiYWNjdCI6MSwiYWNyIjoiMSIsImFpbyI6IkFVUUF1LzhUQUFBQWFCODVvWXhWS1VXMWZ5bVVBem1yU3RIcXdROWM1R0VxbnQwVVRFUGxnc1RGeDhuclNJdDZaclhkakRXcXFMc1pkbzErNit0T2UrSW45UFh6S2ZObXFBPT0iLCJhbHRzZWNpZCI6IjU6OjEwMDMyMDAxN0IwRDgxOUEiLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6IkRldi1Fa2FhLUNoYXRib3QiLCJhcHBpZCI6IjM0NzAxNWU4LTY0MWYtNDUxYi1iYzYxLTM1ZTY0NmEwZDkxZCIsImFwcGlkYWNyIjoiMSIsImVtYWlsIjoic2h1dmFtLnNlbmd1cHRhQGNlbGViYWx0ZWNoLmNvbSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0L2U0ZTM0MDM4LWVhMWYtNDg4Mi1iNmU4LWNjZDc3NjQ1OWNhMC8iLCJpZHR5cCI6InVzZXIiLCJpcGFkZHIiOiI0OS4zNy4zMi40IiwibmFtZSI6IlNodXZhbSBTZW5ndXB0YSIsIm9pZCI6Ijc4OWEyNGVhLTdmMGItNDE5MC04YzIzLWU3Zjk5NGNiMjNjNSIsInBsYXRmIjoiMyIsInB1aWQiOiIxMDAzMjAwMUMzNjQ5RjgwIiwicmgiOiIwLkFUNEE4YnlDeU1lb2xVS0c3VUZvbHdOZl9lZ1ZjRFFmWkJ0RnZHRTE1a2FnMlIwLUFKZy4iLCJzY3AiOiJDaGF0LkNyZWF0ZSBDaGF0LlJlYWQgQ2hhdC5SZWFkQmFzaWMgQ2hhdC5SZWFkV3JpdGUgQ2hhdE1lc3NhZ2UuUmVhZCBDaGF0TWVzc2FnZS5TZW5kIE9ubGluZU1lZXRpbmdzLlJlYWQgT25saW5lTWVldGluZ3MuUmVhZFdyaXRlIG9wZW5pZCBQcmVzZW5jZS5SZWFkIHByb2ZpbGUgVXNlci5SZWFkIGVtYWlsIiwic2lnbmluX3N0YXRlIjpbImttc2kiXSwic3ViIjoiM0pxTk1IU2psMnNadDNHUEgtV0xjbnZZOEt3TDlIQUhVcXRWc2h6RV96cyIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJBUyIsInRpZCI6ImM4ODJiY2YxLWE4YzctNDI5NS04NmVkLTQxNjg5NzAzNWZmZCIsInVuaXF1ZV9uYW1lIjoic2h1dmFtLnNlbmd1cHRhQGNlbGViYWx0ZWNoLmNvbSIsInV0aSI6IkNGSmRGVEFyTFVDdWhKZWVNV216QUEiLCJ2ZXIiOiIxLjAiLCJ3aWRzIjpbIjEzYmQxYzcyLTZmNGEtNGRjZi05ODVmLTE4ZDNiODBmMjA4YSJdLCJ4bXNfc3QiOnsic3ViIjoiNkpYN2liU3BzcnlLQlFWWjZXSXR0UE5tRTRTdmZDclFQQWVsb2RLTUhxRSJ9LCJ4bXNfdGNkdCI6MTU2NTk1OTE2M30.i_hHW7ZIfzxNeZAH--UZAmgZsj69qq_MDqIpQkH_k8KOXfSSFJWO6n_xPP5vnmDJ-GiAZNozrenRqRMWVxnu6bnASPPHYceg72roqEhjg20-9F9lMufzmgpX1iZU__uUDN29NHnWvpujWb8bNt4h7ZDlAB2SZkh_dyIytqlT8ORCr1wFqvtLgAOWCF_yk6GHpKyI1awBBJhIPfcizk78pnT6DqLvi96WxG68Se4bsjhg3urOHlmnpAx_dw4vLyhFMmOaTGZZqUXqnzoSmsOpyYAFTru82bsfRGzt3-Gil1t7XlWEEwo75PM3ShKrcpBvKp-TcqX_HvFl3CtZbyp5LQ",
        "Content-Type": "application/json",
      },
      data: chat,
    };
    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  createChat,
};
