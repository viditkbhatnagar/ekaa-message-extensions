const axios = require("axios");
const { postReq, getReq } = require("../");

module.exports.createChat = async (token) => {
  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    let response = await axios.get(
      `https://graph.microsoft.com/beta/users/fd39393d-5e20-4e63-93d8-b3aef545d062/presence`,
      config
    );
    if (response && response.data) {
      console.log(response.data);
    } else {
      console.log("Else", response.data.error);
    }
  } catch (error) {
    console.log("Catch", error);
  }
};
