const axios = require("axios");

const postRequest = async (url, body, config) => {
  try {
    const response = await axios.post(url, body, config);
    if (response && response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error.message);
  }
};
const getRequest = async (url, config) => {
  let response = await axios.get(url, config);
  if (response && response.data) {
    console.log(response.data);
  } else {
    console.log(error);
  }
};
module.exports = {
  postReq: postRequest,
  getReq: getRequest,
};
