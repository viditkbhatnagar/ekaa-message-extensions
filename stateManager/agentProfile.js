class agentProfile {
  constructor() {
    this.AGENT_ID = null;
    this.AGENT_EMAIL = null;
    this.AGENT_F_NAME = null;
    this.AGENT_L_NAME = null;
    this.AGENT_TOTAL_TICKET = null;
    this.AGENT_DEPT = null;
  }
}

module.exports = {
  AgentProfile: agentProfile,
  UserProfileId: "UserInfo",
};
