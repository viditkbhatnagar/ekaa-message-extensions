class UserProfile {
  constructor() {
    this.USER_ID = null;
    this.USER_EMAIL = null;
    this.USER_F_NAME = null;
    this.USER_L_NAME = null;
    this.USER_TOKEN = null;
  }
}

module.exports = {
  UserProfile: UserProfile,
  UserProfileId: "UserInfo",
};
