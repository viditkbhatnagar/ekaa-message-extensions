module.exports.AgentNotification= (agent_name,username, ticket_id, title, description, ticket_status ) => {
  try {
      let card = 
      {
        "type": "AdaptiveCard",
        "body": [
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "width": "auto",
                        "items": [
                            {
                                "type": "Image",
                                "url": "https://media-exp1.licdn.com/dms/image/C510BAQEj_0svnI6_0w/company-logo_200_200/0/1587024155620?e=2159024400&v=beta&t=6Bq9RqwrMBZGnEX-RQWZmnKU03XP4PT22LcxuT_aemA",
                                "size": "Small"
                            }
                        ]
                    },
                    {
                        "type": "Column",
                        "width": "stretch",
                        "items": [
                            {
                                "type": "TextBlock",
                                "wrap": true,
                                "text": `Hi ${agent_name}, you have been assigned a new ticket.`,
                                "weight": "Bolder",
                                "color": "Accent"
                            },
                            {
                                "type": "TextBlock",
                                "wrap": true,
                                "text": "Here are the details ",
                                "spacing": "None",
                                "color": "Accent",
                                "weight": "Bolder"
                            }
                        ]
                    }
                ]
            },
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "items": [
                            {
                                "type": "ColumnSet",
                                "columns": [
                                    {
                                        "type": "Column",
                                        "width": "auto",
                                        "items": [
                                            {
                                                "type": "FactSet",
                                                "facts": [
                                                    {
                                                        "title": "Raised By  ",
                                                        "value": `${username}`
                                                    },
                                                    {
                                                        "title": "Raised On ",
                                                        "value":`${new Date().toLocaleDateString()}`
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                "spacing": "None"
                            }
                        ],
                        "width": "stretch"
                    }
                ],
                "separator": true
            },
            {
                "type": "FactSet",
                "facts":  [
                        {
                            "title": "Ticket ID",
                            "value": `${ticket_id}`
                        },
                        {
                            "title": "Title",
                            "value": `${title}`
                        },
                        {
                            "title": "Description",
                            "value": `${description}`
                        }
                    ],
                "separator": true
            },
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "width": "74px",
                        "items": [
                            {
                                "type": "TextBlock",
                                "wrap": true,
                                "text": "Status",
                                "color": "Default",
                                "weight": "Bolder"
                            }
                        ]
                    },
                    {
                        "type": "Column",
                        "width": "112px",
                        "items": [
                            {
                                "type": "TextBlock",
                                "wrap": true,
                                "text":`${ticket_status}`,
                            }
                        ]
                    }
                ],
                "separator": true
            }
        ],
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.4"
    }
      return card;
  } catch (error) {
    console.log(error);
  }
}


