// const { SorryImage } = require("../cards/imageBase64.js");
module.exports.SorryCard = (question) => {
  try {
    let card = {
      type: "AdaptiveCard",
      $schema: "http://adaptivecards.io/schemas/adaptive-card.json",
      version: "1.4",
      body: [
        {
          type: "ColumnSet",
          columns: [
            {
              type: "Column",
              width: "stretch",
              items: [
                {
                  type: "TextBlock",
                  wrap: true,
                  text: `Your Query : ${question} `,
                  weight: "Bolder",
                },
              ],
              style: "emphasis",
            },
          ],
        },
        {
          type: "TextBlock",
          wrap: true,
          text: "Sorry , I didn't get your query! You can check below suggestions.",
        },
      ],
    };

    return card;
  } catch (error) {
    console.log("error", error);
  }
};
