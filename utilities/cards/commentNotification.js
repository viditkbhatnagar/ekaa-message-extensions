module.exports.commentNotification = (
  agent_name,
  ticket_id,
  title,
  description,
  comments
) => {
  try {
    let card = 
    {
      "type": "AdaptiveCard",
      "body": [
          {
              "type": "ColumnSet",
              "columns": [
                  {
                      "type": "Column",
                      "width": "auto",
                      "items": [
                          {
                              "type": "Image",
                              "url": "https://media-exp1.licdn.com/dms/image/C510BAQEj_0svnI6_0w/company-logo_200_200/0/1587024155620?e=2159024400&v=beta&t=6Bq9RqwrMBZGnEX-RQWZmnKU03XP4PT22LcxuT_aemA",
                              "size": "Small"
                          }
                      ]
                  },
                  {
                      "type": "Column",
                      "width": "stretch",
                      "items": [
                          {
                              "type": "TextBlock",
                              "size": "Medium",
                              "weight": "Bolder",
                              "style": "heading",
                              "text": "Comment Added Notification",
                              "color": "Accent"
                          }
                      ],
                      "verticalContentAlignment": "Center"
                  }
              ]
          },
          {
              "type": "ColumnSet",
              "columns": [
                  {
                      "type": "Column",
                      "width": "auto",
                      "items": [
                          {
                              "type": "FactSet",
                              "facts": [
                                  {
                                      "title": "Agent Name ",
                                      "value": `${agent_name}`
                                  },
                                  {
                                      "title": "Date",
                                      "value": `${new Date().toLocaleDateString()}`
                                  }
                              ]
                          }
                      ]
                  }
              ]
          },
          {
              "type": "FactSet",
              "facts": [
                  {
                      "title": "Ticket ID",
                      "value": `${ticket_id}`
                  },
                  {
                      "title": "Title",
                      "value": `${title}`
                  },
                  {
                      "title": "Description",
                      "value": `${description}`
                  }
              ],
              "separator": true
          },
          {
              "type": "ColumnSet",
              "columns": [
                  {
                      "type": "Column",
                      "width": "auto",
                      "items": [
                          {
                              "type": "TextBlock",
                              "wrap": true,
                              "weight": "Bolder",
                              "color": "Dark",
                              "text": "Comment :",
                              "spacing": "Small"
                          }
                      ]
                  }
              ],
              "separator": true
          },
          {
              "type": "TextBlock",
              "wrap": true,
              "text": `${comments}`,
              "spacing": "None"
          }
      ],
      "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
      "version": "1.4"
  }
    return card;
  } catch (error) {
    console.log("error", err);
  }
};
