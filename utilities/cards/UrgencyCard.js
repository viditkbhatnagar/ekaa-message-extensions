// const { SorryImage } = require("../cards/imageBase64.js");
module.exports.UrgencyCard = (question) => {
    try {
      let card =
      {
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "type": "AdaptiveCard",
        "version": "1.4",
        
        "body": [
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "width": "auto",
                        "items": [
                            {
                                "type": "Image",
                                "url": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhIVFRUVFxUXGBgXFxUXFxYXFxcXGBgaHRUYHSggGB0lHRcXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lICUtLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMgAyAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwEDBAUGAgj/xABGEAABAgMDBwcICQMEAwEAAAABAAIDEbEEBSEGEjFBUWHwBxMicYGRoTJSU1RyktHhFBUWF0KCk8HxIzNiQ6LS06OywjT/xAAaAQACAwEBAAAAAAAAAAAAAAAABAMFBgIB/8QANBEAAgECAwQIBQQDAQAAAAAAAAECAxEEEiEFMUFRE2FxgZGhsdEUMsHh8BUiQlJicvEj/9oADAMBAAIRAxEAPwCcUREAEREAEREAERaW98prNZsIsQZ3mN6T+4aO2S8clFXZ3CnKcssE2+SN0ije8eU7GUCB+aI7R+VvxXPWzLu2xP8AVEMbGNaPEzcO9LyxVNbtSypbGxM96Ue1/RJk0ooBiX7aX+VaIzp6jEeQPFYptjz/AKj95mVH8auEfMaWwZ8Zrw+6PohF8+wr2jtxbGiN2Se4UKz7Llfbofk2hx258n/+4KFjY8Ucy2FU/jNd917k5oorsHKZGb/ehMcNrZscezEUXU3Tl3ZI0mucYTjqieT74wHbJTxxFOW5iNbZmJpauN11a+mvkdWitw4ocAWkEHQRiD2q4phAIiIAIiIAIiIAIiIAIiIAIiIALS39lJAsg/qum6WDG4vPZqG8rmsrsuxDzoVmILxgYmlrTsaPxHfo69UZ2iO57i97iXOMyXEkz2klJ1sUo6Q19C6wOyJVUp1tFy4v29eO46S/cubTHmGnmoZ0MYTnEf5O0nskuXJx36ylaJSqQlJyd5O5pKVGnSjlpqy/O994pVK0TjqTglcko4JTgBOAErrOzjwQArRKVSlUrRACtE4JTglOAEAbO6L/ALRZjOFELR5mlp62nDuUj5OZfQo0mRgIUTbP+m47ifJ6j3lRLXWdiUqpqdadPdu5CWKwFHE/MrPmt/s+8+jWOBxC9KGclssYtmIY+cSF5utg/wAT+1FLF2XlDtDBEhODgdisqVaNRaGVxmCqYWVpap7nwfs+pmciIpRMIiIAIiIAIiIAKMcvMss4us9mfJgwiRAfK/wafN2kadGic9nyiZTc002aE6URw6bh+Bh1e0fAdYUVn+AkcTXt+yPeaDZOzlJKvVX+q+r+nPfusOAErRK0SlUgaMUqlaJWicEoAcEpwAnACV27EAK6zsSlUpVK0QArROCU4JVOAEAV4ATgnYlduxKVQApVK0StE4J2oApTWdq3GTt/RLJEBaTmnym7RtWo4ATglexk4u6OKlOFSLhNXT/P+PgT7ct7Q7TDD2Ga2SgzJa/nWWIDP+mToU0XfbWxWBzSrajVVSN+PExmOwUsLUy709z/ADivZ8TLREUwkEREAFqMpL3bZLO+M7EjBo8550D9zuBW3UQcpd887aOZaZsgzHXEPlHs8nsKhr1OjhfiO4DC/EVlF7lq+z7nK2q0uiPdEiHOLiXHeTpKs1olaJSqqDapWVkKVSurclaLKu6wRI8RsKE0kng46hvQDaSuzF4JTgBShdnJrCDR9IiPe7zWSDBhomWzd14dS2f3fWHzH++UysJUKuW2sLF2V32L3a9CHK693HglKqY/u9sPmP8Afcn3e2H0b/fK9+EqdRz+t4blLwXuQ5WicEqY/u9sPmP99yfd7YfMf75R8JU6g/W8Nyl4L3Ic4ASus7OPBSte+RVhgwIsXNcMxjnAlzpTAw8ZKKZjsqoalKVNpSHMLjKeJTcE9Oa+7FKpWiVolNZ2qMbHHWnACcAJwSgBXWUpVKVStEAK0Xdcn+UBYeacZgaOr5LhaaztV2yxyxzXt0zmApaVTo5ZhXG4VYmi4ceHb99x9DtMxMa17XO5I3oI0ICeoEdS6JXBh2mnZhERB4a6/bcIFniRj+BpI3u0NHfJQJFeXOJJmSSSd5Un8q1vzYEOCD/ccXO9lg+JB/KospVVuLneeXkarYlHJRdTjJ+S09blaVTjqStEH8lKFyOCVK/JfdQZZzHI6cUkA4YMaZYHeQSeobFFGseAU/3LY+ZgQoWE2Ma0y0Z0uke0zKbwcLzzcil25WcaMaa/k/Jfdo2CIoUyn5QrYLVGZZ4wZDY9zGgQ4Z8kyJJe0mcwdyfnUUN5ncPhp121DhzJrRQB94t5etD9KD/1qv3jXl6z/wCKD/wUXxMRv9Krc4+L9ifkUAfeJefrQ/Sg/wDWq/eLeXrI/Sgf8EfExD9Lq84+L9iS+Va381YS0EgxXshiXa8g7iGEdqhqzvJ66K9fWUdqtmYLRF5zNmWjNY0DOlM9BonoGlWrM3DjFKYiam7ou9m0JUYZXvu2X6aztVeAE4ASus7EqWw4J2KlKqtKpWiAFaJSqpSqrwAgBwAnBOxOCUpVAHZZA3nmOzdQPgfnVS2x0xNQDckfMjNO3DqmptyftXOQWnYJK1ws81O3LQx+16PR4ltbpa+/nqbNERMFYRDyo2rOtQbPBkNo7SS74Ljq0W+y4i51ui65EAdzfitDwSqes71JdpucDHLhqa/xXmr/AFHBKcAJwAlaKIaNrknZOdtkFmnptc7WJM6RHbmy7VPSibknsedaIkSWEOHKe9xkPAOUsqzwkbQvzZk9tVM2Iy8kvPX0aMG+LYIECLGImIcN75bc1pMu2Ul80SLiSSSTi4nSSdJ7VN/KzbebsDmYTivYzTjIHPMtvkgdqheys+S4xUtUhjY9L9kpc36f9LXMn4BOYK2QZ36zs48EzR2VSecvOgRrOY7qqvMHjUtlm99EzB2bdqM4dCjFsd2xIk+bY98pTzWl0p6NHUe5bWFc0cD+xE3DMf8ABSdyUWIMsr4shOLEOOvNZgAfzZ5/Mu4TUMNnim2UtbaroVpQjFOztc+ffqiP6KLPbmP+CfVMf0EWXsPx8F9BIuvglzOP16f9F4s+ffqiP6GJP2H4eCfVEf0MXf0HY+C+gkR8Ev7B+vT/AKLxPn36oj+hibhmP+CfVMf0MXecx3wX0EsG97cIECJFMugxzsdBIGA7TIdqHgo8wW3aj0VNeLPn9wlgRKWEtcwqVovIjTM5zJXqlVXmkTPcF8iDvB69al/Ie0zaWqHh/AUl5AxukN7RRPYJ/MuwoNvR0py7V6P3JDRET5nCBsrP/wBkf2zPwWp4AW5ywZK2Rd5BA7G/BaatFS1PnfazeYV3oU/9Y+iFde5UpVVHhVCe+i4JyWeSyw5lkdFIxjRCR7DeiB72f3rtlqsm7JzVlgQ9kNs95ImfElbVXVOOWCRg8VV6WtOfNsiTlqt04lngAjotdEI3uOa3H8rlwVlb36zsXTcodgtMW8IruZilvQa05ji3MDB5LgJEE5x3EkaVqoN0RgP7MSXsOx8FXV7ub0NLs3JGjFXW7muJj0qlaLM+qo/oYk/Ydh4J9VRvQxN/Qd8FBlfItOlhzXijDprO1UJ+QWb9VxvQxNwzHfBYtqguYZPaWmU+kCMO1eWZ6pxe5kuZO5RWKBZYMJ1qhBzWNzsZSccXTA3mS2X2ysHrcLvXz3HPdVWq0T8cQ0rWM7PZcJSbcnr2H0bZ8qrFEe2Gy0w3PcQGtBmSToC3agfkpsJi29jpGUJr3k75Zo7JuU8JilNzjdlVi6EaFTJF30LNpjthsc95DWtBc4nQABMk9i032ysHrcL3lquVa3c3d72ggGI5jNMjKeeZe7LtUFT7lxVrOErJDGEwMa8HOTa1Por7ZWD1uF7y5flFyssz7E+FZ47HviOY0huJzZ5xO7QFD1aIBxtUUsQ2mrDtPZkITUsz0dzLsvgs7gBY9mb8gsjglIy3mgpq0So/krv8hX/1G+yKLgBp3VXe5Dt/qM3NFE3gvml2fUpdvP8A84LrfoSe3QiM0IrAzRDGX8HNtOd5zAZ9Ux+y5mlV3vKTY8GxPNcWnqMyPEf7lwVaKpxMbVH4mz2XUz4WHVp4P2sK0Xl5wNV64JVuNooFAPvcT5kvb2x7LBit0FjQdzm9Fw7CCFt1AuRmWL7A8tc0vgPM3tBE2nRnNnhOWkYTkMQpWseW9giNDhaobdzzzbh+V0j26Fb06qlHV6mKxeDqUpuyuuDOkRaT7W2H1yB+o1PtbYfXIH6jfipcy5imSXJm7RaT7W2H1yB+o34p9rbD65A/UajMuYZJcmbtQdyiXkI1uiYzbDlCGEvInnDseX4qUo2V9ia0uFrgukCZCI2ZkNAx0qAotpdFiOiOJLnOc4k6ZuJJnvxSuKknFIt9j03GrKbXC3j9ke3QJ9dFT6MOyqyWDDdr3qpPyCr8zNMqaZI/I5dubDjRyB0nNht2gNGc7HYc5nuqSVosirEYNigMM5lmeZ6QXkvl2Z0uxb1W9GOWCRicZUVSvOS3X07FoiJeWu3TfZ4E9DXRCPaOa3H8r1HUGBProt5l7bufvCMdTXc23GYAhjNMtxIc78y10AYbqqvrzvJs0uzqKjRinyv46ln6N3VVRZvkFlcAJwSoMzLHo4nmG2X7leuOtKVStFySJWPTBN0tdFIuQ8Ppk7AuAsDJvGwYnepOyIgdEu2lWGCjaLf5oZnbtS9SEFwV/F+yO3ZoRVaidKI5HK2xc5DezaMOsYjxAUQOb81PF8QZhRDlbYOajFwHRiTI3O/F8e1JYyF0pF7sPEZZyovjqu1b/L0NJwAqOHfrOxVrrOxKVVeaYwLRC7qrGPG5bZzJ9dFYdZx81Ipi06Wt0a9OAFnfRvkE+jfNdZkR9FIwZpNZv0buXr6N30RmR70UjAn30WTZW91Vc+jcbVfhQpfsF45Kx1Cm76lwfwFk3fZjFiw4QlN72MmdHScB+6x+CV1fJpYedtoeR0YLXO0TBcQGtB2eWSPZXNOOaSid4mr0NGVTkn48PMl+DDDWhrRINAAGwDAKxelrEGDEikgBjHOx0dEErMXG8qlv5q73iZBiuZCEt5LnA7ixjh2q5lKybMNShnnGHN2IRY7OcXHWST1kzWewfILCsrfkFnD+SqabNzRVkV4JSlUpVK0XBOK0SlUpVemNJIGvUEA2krs2N0wScduHfp43KW8mbNmQ2jcuAybsOc9o1NUp3dCk0K6pwyQUeRhMVX6etKpzenZuXkZqIi7FyxaocwuFynurnWOZoOlp2OH7alIBC0d8WWeK8aTVmdQnKElKO9aog+IwtJBBEjIjXMLzWi6zKu59MZgxHljd53Zr+WPJ01naqerTdOWVm4wmKjiaSnHvXJ/mtxwTtTgBOAE4JUYyOCdiUqqUqq1ogBWicdapSqrwAgBwAnBKcEqlKoAUqpO5JbDKFFjkYvc1gx1NGdKWrFyjI+NF0dy8oT7HAbAZAY4AuJcSQXFziZkAacQOxMYZxjO7K7akKlTD5Kau21fVLTfx67E2KI+Wm3Z0WBAH4WuecdbyAJjcG/7l4+96N6sz3nfBcZlDfb7baHR3tDSQ0SGIaGiUp69femq1aMo2RS4HBVadZSqKyV+KfoWLK35lZVKq3AGG6quVoq5mpgrIVoqUqq0qnAC8OhwAtjdtn16zgP3KxbJAzju1n4Lr8n7uziHEYDQncJSu877ih2xjcseghvfzdS5d/p26dFktd2aBPSV2kJsgtfddmkFs1YGaCIiACs2iFnBXkQByF52SRUf5RXHmExIY6Oto/CdvUpit1lDguWt1kzScFHVpKpGzGcJi54apnh3rg1+bnw8URNwSqUqunvvJ6U3whhpLB/8APw7ti5pwx30VVUpSg7SNjhcXTxMM1N9q4rt/NSlaJTWdqUqnACjGRwAnBKcEpSqAKUqq1olaJSqAPLtG6qwI7TOi2J/gLwWfMrpOxHUhmRq+aPxKuwYSz+aHGtVDO+i6cyNUbMMHfReqVVKVVeAFGMDgBXbPALzIdp41L1ZrIXdWs/sF0d1XVnSAEm1TVDDOest3qVG0NqRoJwp6z8l9+rx5PzdF250gB0R4qQLmu+UsMAsW6bt0ADBdTZYAaFZpW3GUbbbb3l2G2QXtEQeBERABERAFCFgW6xBwWwRAHF2yxlpXPXrcbI0z5L/OGvrGtSVarIHLQW27SNC8lFSVmd06k6cs0HZkT2+64kE9JsxqIxCwiPmVKEaz6iFpLfk7Dfi0ZvVoPYkqmD/o+5l9htucK8e9fVez7jiqVStFuLVk/EZiJO8Frn2N7cMx3dOiVlRqR3ot6WPw1T5Zrvdn4OzLFKpwAqlu6Z1BM3r3lRDad9xTglKat6qR1y1b17bAcdDSex0gvUm9xzKpGPzNLtdi3WiUqspl3uOoDr0+C2FmuYnUT4BTQw1SXC3b+XEK21cLT/lfs1893mahkMnQMdmzrWxsd2knHEnUFv7LcktMgNgW7sd3gYNCcp4WEdXq/Io8VtetVWWH7V1b/H28TU3dc+gu7Auou67pywwWZYbs2reQLOGhNFSeLJZg0LKREAEREAEREAEREAEREAFbfCBVEQBr7VdoK1FpushEQBgRLKRqWNEsoOloREAY0S7WH8Ksm5IexEXoWQbcrBtVxl1M2Eoi8PLIyYVgaNDVmwrEToCIg9NjZbqJ0rcWa7w3UiIAzWsAXtEQAREQAREQAREQB//Z",
                                "size": "Small"
                            }
                        ]
                    },
                    {
                        "type": "Column",
                        "width": "stretch",
                        "items": [
                            {
                                "type": "TextBlock",
                                "size": "Large",
                                "weight": "Bolder",
                                "wrap": true,
                                "style": "heading",
                                "text": "Select Urgency"
                            }
                        ],
                        "verticalContentAlignment": "Center"
                    }
                ]
            },
            {
                "type": "TextBlock",
                "isSubtle": true,
                "wrap": true,
                "text": "Can you please select urgency from below options so can we assign it to the correct team.",
                "separator": true
            },
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "width": "auto",
                        "items": [
                            {
                                "type": "ActionSet",
                                "actions": [
                                    {
                                        "type": "Action.Submit",
                                        "title": "Low",
                                        data: {
                                            action: "Urgency",
                                            value : "Low",
                                            msteams: {
                                              type: "messageBack",
                                              displayText: "Low",
                                            },
                                          },
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "type": "Column",
                        "width": "auto",
                        "items": [
                            {
                                "type": "ActionSet",
                                "actions": [
                                    {
                                        "type": "Action.Submit",
                                        "title": "Medium",
                                        data: {
                                            action: "Urgency",
                                            value : "Medium",
                                            msteams: {
                                              type: "messageBack",
                                              displayText: "Medium",
                                            },
                                          },
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "type": "Column",
                        "width": "auto",
                        "items": [
                            {
                                "type": "ActionSet",
                                "actions": [
                                    {
                                        "type": "Action.Submit",
                                        "title": "High",
                                        data: {
                                            action: "Urgency",
                                            value : "High",
                                            msteams: {
                                              type: "messageBack",
                                              displayText: "High",
                                            },
                                          },
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
      return card;
    } catch (error) {
      console.log("error", error);
    }
  };
module.exports.updatedUrgencyCard = (question) => {
    try {
      let card =
      {
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "type": "AdaptiveCard",
        "version": "1.4",
        
        "body": [
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "width": "auto",
                        "items": [
                            {
                                "type": "Image",
                                "url": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhIVFRUVFxUXGBgXFxUXFxYXFxcXGBgaHRUYHSggGB0lHRcXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lICUtLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMgAyAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwEDBAUGAgj/xABGEAABAgMDBwcICQMEAwEAAAABAAIDEbEEBSEGEjFBUWHwBxMicYGRoTJSU1RyktHhFBUWF0KCk8HxIzNiQ6LS06OywjT/xAAaAQACAwEBAAAAAAAAAAAAAAAABAMFBgIB/8QANBEAAgECAwQIBQQDAQAAAAAAAAECAxEEEiEFMUFRE2FxgZGhsdEUMsHh8BUiQlJicvEj/9oADAMBAAIRAxEAPwCcUREAEREAEREAERaW98prNZsIsQZ3mN6T+4aO2S8clFXZ3CnKcssE2+SN0ije8eU7GUCB+aI7R+VvxXPWzLu2xP8AVEMbGNaPEzcO9LyxVNbtSypbGxM96Ue1/RJk0ooBiX7aX+VaIzp6jEeQPFYptjz/AKj95mVH8auEfMaWwZ8Zrw+6PohF8+wr2jtxbGiN2Se4UKz7Llfbofk2hx258n/+4KFjY8Ucy2FU/jNd917k5oorsHKZGb/ehMcNrZscezEUXU3Tl3ZI0mucYTjqieT74wHbJTxxFOW5iNbZmJpauN11a+mvkdWitw4ocAWkEHQRiD2q4phAIiIAIiIAIiIAIiIAIiIAIiIALS39lJAsg/qum6WDG4vPZqG8rmsrsuxDzoVmILxgYmlrTsaPxHfo69UZ2iO57i97iXOMyXEkz2klJ1sUo6Q19C6wOyJVUp1tFy4v29eO46S/cubTHmGnmoZ0MYTnEf5O0nskuXJx36ylaJSqQlJyd5O5pKVGnSjlpqy/O994pVK0TjqTglcko4JTgBOAErrOzjwQArRKVSlUrRACtE4JTglOAEAbO6L/ALRZjOFELR5mlp62nDuUj5OZfQo0mRgIUTbP+m47ifJ6j3lRLXWdiUqpqdadPdu5CWKwFHE/MrPmt/s+8+jWOBxC9KGclssYtmIY+cSF5utg/wAT+1FLF2XlDtDBEhODgdisqVaNRaGVxmCqYWVpap7nwfs+pmciIpRMIiIAIiIAIiIAKMcvMss4us9mfJgwiRAfK/wafN2kadGic9nyiZTc002aE6URw6bh+Bh1e0fAdYUVn+AkcTXt+yPeaDZOzlJKvVX+q+r+nPfusOAErRK0SlUgaMUqlaJWicEoAcEpwAnACV27EAK6zsSlUpVK0QArROCU4JVOAEAV4ATgnYlduxKVQApVK0StE4J2oApTWdq3GTt/RLJEBaTmnym7RtWo4ATglexk4u6OKlOFSLhNXT/P+PgT7ct7Q7TDD2Ga2SgzJa/nWWIDP+mToU0XfbWxWBzSrajVVSN+PExmOwUsLUy709z/ADivZ8TLREUwkEREAFqMpL3bZLO+M7EjBo8550D9zuBW3UQcpd887aOZaZsgzHXEPlHs8nsKhr1OjhfiO4DC/EVlF7lq+z7nK2q0uiPdEiHOLiXHeTpKs1olaJSqqDapWVkKVSurclaLKu6wRI8RsKE0kng46hvQDaSuzF4JTgBShdnJrCDR9IiPe7zWSDBhomWzd14dS2f3fWHzH++UysJUKuW2sLF2V32L3a9CHK693HglKqY/u9sPmP8Afcn3e2H0b/fK9+EqdRz+t4blLwXuQ5WicEqY/u9sPmP99yfd7YfMf75R8JU6g/W8Nyl4L3Ic4ASus7OPBSte+RVhgwIsXNcMxjnAlzpTAw8ZKKZjsqoalKVNpSHMLjKeJTcE9Oa+7FKpWiVolNZ2qMbHHWnACcAJwSgBXWUpVKVStEAK0Xdcn+UBYeacZgaOr5LhaaztV2yxyxzXt0zmApaVTo5ZhXG4VYmi4ceHb99x9DtMxMa17XO5I3oI0ICeoEdS6JXBh2mnZhERB4a6/bcIFniRj+BpI3u0NHfJQJFeXOJJmSSSd5Un8q1vzYEOCD/ccXO9lg+JB/KospVVuLneeXkarYlHJRdTjJ+S09blaVTjqStEH8lKFyOCVK/JfdQZZzHI6cUkA4YMaZYHeQSeobFFGseAU/3LY+ZgQoWE2Ma0y0Z0uke0zKbwcLzzcil25WcaMaa/k/Jfdo2CIoUyn5QrYLVGZZ4wZDY9zGgQ4Z8kyJJe0mcwdyfnUUN5ncPhp121DhzJrRQB94t5etD9KD/1qv3jXl6z/wCKD/wUXxMRv9Krc4+L9ifkUAfeJefrQ/Sg/wDWq/eLeXrI/Sgf8EfExD9Lq84+L9iS+Va381YS0EgxXshiXa8g7iGEdqhqzvJ66K9fWUdqtmYLRF5zNmWjNY0DOlM9BonoGlWrM3DjFKYiam7ou9m0JUYZXvu2X6aztVeAE4ASus7EqWw4J2KlKqtKpWiAFaJSqpSqrwAgBwAnBOxOCUpVAHZZA3nmOzdQPgfnVS2x0xNQDckfMjNO3DqmptyftXOQWnYJK1ws81O3LQx+16PR4ltbpa+/nqbNERMFYRDyo2rOtQbPBkNo7SS74Ljq0W+y4i51ui65EAdzfitDwSqes71JdpucDHLhqa/xXmr/AFHBKcAJwAlaKIaNrknZOdtkFmnptc7WJM6RHbmy7VPSibknsedaIkSWEOHKe9xkPAOUsqzwkbQvzZk9tVM2Iy8kvPX0aMG+LYIECLGImIcN75bc1pMu2Ul80SLiSSSTi4nSSdJ7VN/KzbebsDmYTivYzTjIHPMtvkgdqheys+S4xUtUhjY9L9kpc36f9LXMn4BOYK2QZ36zs48EzR2VSecvOgRrOY7qqvMHjUtlm99EzB2bdqM4dCjFsd2xIk+bY98pTzWl0p6NHUe5bWFc0cD+xE3DMf8ABSdyUWIMsr4shOLEOOvNZgAfzZ5/Mu4TUMNnim2UtbaroVpQjFOztc+ffqiP6KLPbmP+CfVMf0EWXsPx8F9BIuvglzOP16f9F4s+ffqiP6GJP2H4eCfVEf0MXf0HY+C+gkR8Ev7B+vT/AKLxPn36oj+hibhmP+CfVMf0MXecx3wX0EsG97cIECJFMugxzsdBIGA7TIdqHgo8wW3aj0VNeLPn9wlgRKWEtcwqVovIjTM5zJXqlVXmkTPcF8iDvB69al/Ie0zaWqHh/AUl5AxukN7RRPYJ/MuwoNvR0py7V6P3JDRET5nCBsrP/wBkf2zPwWp4AW5ywZK2Rd5BA7G/BaatFS1PnfazeYV3oU/9Y+iFde5UpVVHhVCe+i4JyWeSyw5lkdFIxjRCR7DeiB72f3rtlqsm7JzVlgQ9kNs95ImfElbVXVOOWCRg8VV6WtOfNsiTlqt04lngAjotdEI3uOa3H8rlwVlb36zsXTcodgtMW8IruZilvQa05ji3MDB5LgJEE5x3EkaVqoN0RgP7MSXsOx8FXV7ub0NLs3JGjFXW7muJj0qlaLM+qo/oYk/Ydh4J9VRvQxN/Qd8FBlfItOlhzXijDprO1UJ+QWb9VxvQxNwzHfBYtqguYZPaWmU+kCMO1eWZ6pxe5kuZO5RWKBZYMJ1qhBzWNzsZSccXTA3mS2X2ysHrcLvXz3HPdVWq0T8cQ0rWM7PZcJSbcnr2H0bZ8qrFEe2Gy0w3PcQGtBmSToC3agfkpsJi29jpGUJr3k75Zo7JuU8JilNzjdlVi6EaFTJF30LNpjthsc95DWtBc4nQABMk9i032ysHrcL3lquVa3c3d72ggGI5jNMjKeeZe7LtUFT7lxVrOErJDGEwMa8HOTa1Por7ZWD1uF7y5flFyssz7E+FZ47HviOY0huJzZ5xO7QFD1aIBxtUUsQ2mrDtPZkITUsz0dzLsvgs7gBY9mb8gsjglIy3mgpq0So/krv8hX/1G+yKLgBp3VXe5Dt/qM3NFE3gvml2fUpdvP8A84LrfoSe3QiM0IrAzRDGX8HNtOd5zAZ9Ux+y5mlV3vKTY8GxPNcWnqMyPEf7lwVaKpxMbVH4mz2XUz4WHVp4P2sK0Xl5wNV64JVuNooFAPvcT5kvb2x7LBit0FjQdzm9Fw7CCFt1AuRmWL7A8tc0vgPM3tBE2nRnNnhOWkYTkMQpWseW9giNDhaobdzzzbh+V0j26Fb06qlHV6mKxeDqUpuyuuDOkRaT7W2H1yB+o1PtbYfXIH6jfipcy5imSXJm7RaT7W2H1yB+o34p9rbD65A/UajMuYZJcmbtQdyiXkI1uiYzbDlCGEvInnDseX4qUo2V9ia0uFrgukCZCI2ZkNAx0qAotpdFiOiOJLnOc4k6ZuJJnvxSuKknFIt9j03GrKbXC3j9ke3QJ9dFT6MOyqyWDDdr3qpPyCr8zNMqaZI/I5dubDjRyB0nNht2gNGc7HYc5nuqSVosirEYNigMM5lmeZ6QXkvl2Z0uxb1W9GOWCRicZUVSvOS3X07FoiJeWu3TfZ4E9DXRCPaOa3H8r1HUGBProt5l7bufvCMdTXc23GYAhjNMtxIc78y10AYbqqvrzvJs0uzqKjRinyv46ln6N3VVRZvkFlcAJwSoMzLHo4nmG2X7leuOtKVStFySJWPTBN0tdFIuQ8Ppk7AuAsDJvGwYnepOyIgdEu2lWGCjaLf5oZnbtS9SEFwV/F+yO3ZoRVaidKI5HK2xc5DezaMOsYjxAUQOb81PF8QZhRDlbYOajFwHRiTI3O/F8e1JYyF0pF7sPEZZyovjqu1b/L0NJwAqOHfrOxVrrOxKVVeaYwLRC7qrGPG5bZzJ9dFYdZx81Ipi06Wt0a9OAFnfRvkE+jfNdZkR9FIwZpNZv0buXr6N30RmR70UjAn30WTZW91Vc+jcbVfhQpfsF45Kx1Cm76lwfwFk3fZjFiw4QlN72MmdHScB+6x+CV1fJpYedtoeR0YLXO0TBcQGtB2eWSPZXNOOaSid4mr0NGVTkn48PMl+DDDWhrRINAAGwDAKxelrEGDEikgBjHOx0dEErMXG8qlv5q73iZBiuZCEt5LnA7ixjh2q5lKybMNShnnGHN2IRY7OcXHWST1kzWewfILCsrfkFnD+SqabNzRVkV4JSlUpVK0XBOK0SlUpVemNJIGvUEA2krs2N0wScduHfp43KW8mbNmQ2jcuAybsOc9o1NUp3dCk0K6pwyQUeRhMVX6etKpzenZuXkZqIi7FyxaocwuFynurnWOZoOlp2OH7alIBC0d8WWeK8aTVmdQnKElKO9aog+IwtJBBEjIjXMLzWi6zKu59MZgxHljd53Zr+WPJ01naqerTdOWVm4wmKjiaSnHvXJ/mtxwTtTgBOAE4JUYyOCdiUqqUqq1ogBWicdapSqrwAgBwAnBKcEqlKoAUqpO5JbDKFFjkYvc1gx1NGdKWrFyjI+NF0dy8oT7HAbAZAY4AuJcSQXFziZkAacQOxMYZxjO7K7akKlTD5Kau21fVLTfx67E2KI+Wm3Z0WBAH4WuecdbyAJjcG/7l4+96N6sz3nfBcZlDfb7baHR3tDSQ0SGIaGiUp69femq1aMo2RS4HBVadZSqKyV+KfoWLK35lZVKq3AGG6quVoq5mpgrIVoqUqq0qnAC8OhwAtjdtn16zgP3KxbJAzju1n4Lr8n7uziHEYDQncJSu877ih2xjcseghvfzdS5d/p26dFktd2aBPSV2kJsgtfddmkFs1YGaCIiACs2iFnBXkQByF52SRUf5RXHmExIY6Oto/CdvUpit1lDguWt1kzScFHVpKpGzGcJi54apnh3rg1+bnw8URNwSqUqunvvJ6U3whhpLB/8APw7ti5pwx30VVUpSg7SNjhcXTxMM1N9q4rt/NSlaJTWdqUqnACjGRwAnBKcEpSqAKUqq1olaJSqAPLtG6qwI7TOi2J/gLwWfMrpOxHUhmRq+aPxKuwYSz+aHGtVDO+i6cyNUbMMHfReqVVKVVeAFGMDgBXbPALzIdp41L1ZrIXdWs/sF0d1XVnSAEm1TVDDOest3qVG0NqRoJwp6z8l9+rx5PzdF250gB0R4qQLmu+UsMAsW6bt0ADBdTZYAaFZpW3GUbbbb3l2G2QXtEQeBERABERAFCFgW6xBwWwRAHF2yxlpXPXrcbI0z5L/OGvrGtSVarIHLQW27SNC8lFSVmd06k6cs0HZkT2+64kE9JsxqIxCwiPmVKEaz6iFpLfk7Dfi0ZvVoPYkqmD/o+5l9htucK8e9fVez7jiqVStFuLVk/EZiJO8Frn2N7cMx3dOiVlRqR3ot6WPw1T5Zrvdn4OzLFKpwAqlu6Z1BM3r3lRDad9xTglKat6qR1y1b17bAcdDSex0gvUm9xzKpGPzNLtdi3WiUqspl3uOoDr0+C2FmuYnUT4BTQw1SXC3b+XEK21cLT/lfs1893mahkMnQMdmzrWxsd2knHEnUFv7LcktMgNgW7sd3gYNCcp4WEdXq/Io8VtetVWWH7V1b/H28TU3dc+gu7Auou67pywwWZYbs2reQLOGhNFSeLJZg0LKREAEREAEREAEREAEREAFbfCBVEQBr7VdoK1FpushEQBgRLKRqWNEsoOloREAY0S7WH8Ksm5IexEXoWQbcrBtVxl1M2Eoi8PLIyYVgaNDVmwrEToCIg9NjZbqJ0rcWa7w3UiIAzWsAXtEQAREQAREQAREQB//Z",
                                "size": "Small"
                            }
                        ]
                    },
                    {
                        "type": "Column",
                        "width": "stretch",
                        "items": [
                            {
                                "type": "TextBlock",
                                "size": "Large",
                                "weight": "Bolder",
                                "wrap": true,
                                "style": "heading",
                                "text": "Select Urgency"
                            }
                        ],
                        "verticalContentAlignment": "Center"
                    }
                ]
            },
            {
                "type": "TextBlock",
                "isSubtle": true,
                "wrap": true,
                "text": "Can you please select urgency from below options so can we assign it to the correct team.",
                "separator": true
            },
           
        ]
    }
      return card;
    } catch (error) {
      console.log("error", error);
    }
  };
  
  
  
  