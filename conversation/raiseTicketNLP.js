module.exports = {
   introduction : {
    "introduction1" : "Hello Shuvam, I am a EKAA Digital Assistant Bot. I will help you raise a ticket.",
    "introduction2" : "Can You Please tell me the problem you are facing in brief.",
   },
   askForTicketIssue : "Can You Please tell me the problem you are facing in brief.",

    askForTicketDescription : "Ok, Can you now please elaborate the issue in details. It will help us to analyze the issue in a better way.",   
}