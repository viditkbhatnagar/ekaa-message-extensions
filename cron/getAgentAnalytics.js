const axios = require("axios");
const CronJob = require("cron").CronJob;
// process.env.TZ = 'Asia/Kolkata'
// console.log(new Date().toString());
const url = "https://ticketappekaa.azurewebsites.net/";
const apiCall = async (agent_id) => {
  console.log("Calling....");
  try {
    const resp = await axios.get(`${url}/Agent/${agent_id}/ticket/`);
    if (resp && resp.data) {
      return {
        status: "Success",
        data: resp.data,
      };
    }
  } catch (err) {
    if (err.response) {
      console.log(err.response.data);
      return {
        status: "Failed",
        error: err.response.data,
      };
    }
  }
};

let ClosedTicket = [];
let InProgressTicket = [];
let OpenTicket = [];
let OnHoldTicket = [];

//Caller Function
exports.Analytics = async (agent_id) => {
  const result = await apiCall(agent_id);
  console.log(result);
  if (result.status == "Success") {
    const allTickets = result.data.data.ticketTables;
    allTickets.map((data) => {
      if (
        data.ticket_state == "1" &&
        new Date(data.updatedAt).toLocaleDateString() ===
          new Date().toLocaleDateString()
      ) {
        if (!OpenTicket.includes(data.ticket.ticket_id)) {
          OpenTicket.push(data.ticket.ticket_id);
        }
      } else if (
        data.ticket_state == "2" &&
        new Date(data.updatedAt).toLocaleDateString() ===
          new Date().toLocaleDateString()
      ) {
        if (!InProgressTicket.includes(data.ticket.ticket_id)) {
          InProgressTicket.push(data.ticket.ticket_id);
        }
      } else if (
        data.ticket_state == "3" &&
        new Date(data.updatedAt).toLocaleDateString() ===
          new Date().toLocaleDateString()
      ) {
        if (!OnHoldTicket.includes(data.ticket.ticket_id)) {
          OnHoldTicket.push(data.ticket.ticket_id);
        }
      } else if (
        data.ticket_state == "4" &&
        new Date(data.updatedAt).toLocaleDateString() ===
          new Date().toLocaleDateString()
      ) {
        if (!ClosedTicket.includes(data.ticket.ticket_id)) {
          ClosedTicket.push(data.ticket.ticket_id);
        }
      }
    });

    let AgentAnalytics = {
      message: `Analytics of Agent on ${new Date().toLocaleDateString()}`,
      new_ticket: OpenTicket.length,
      inprogress_ticket: InProgressTicket.length,
      onhold_ticket: OnHoldTicket.length,
      closed_ticket: ClosedTicket.length,
    };

    console.log(
      `Agent Analytics on ${new Date().toLocaleString()} is : `,
      AgentAnalytics
    );
    return {
      status: "Success",
      data: AgentAnalytics
    }
  } else {
    console.log("Error", result.error);
    return {
      status: "Failed",
      error: result.error
    }
  }
};

// cron.schedule('16 00 * * 1-5',()=>{
// caller()
// })

// new CronJob(
//   "53 9 * * *",
//   () => {
//     console.log("Tik");
//     caller();
//   },
//   null,
//   true,
//   "Asia/Kolkata"
// );
// job.start();
