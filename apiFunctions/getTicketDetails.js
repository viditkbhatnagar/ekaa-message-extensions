const axios = require("axios");
// const url = "https://ticketappekaa.azurewebsites.net/";
const {ticketApplication} = require("../config/config");
const url = ticketApplication.baseUrl;

module.exports.getTicketDetails = async (ticket_id) => {
  try {
    const resp = await axios.get(`${url}/ticket/${ticket_id}`);
    if (resp && resp.data) {
      console.log(resp.data);
    }
  } catch (error) {
    console.log(error);
  }
};
