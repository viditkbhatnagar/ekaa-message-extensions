const axios = require("axios");
const {ticketApplication} = require("../config/config");
const url = ticketApplication.baseUrl;

module.exports.checkIsAgent = async (email) => {
  try {
    // console.log(email);
    const resp = await axios.get(`${url}/Agent/`);
    if (resp && resp.data) {
      // console.log(resp.data);
      const Agents = resp.data.data;
      let result = false;
      for (let i = 0; i < Agents.length; i++) {
        if (Agents[i].email == email) {
          console.log(Agents[i].email);
          console.log("Agent");
          result = true;
        }
      }

      return result;

      //   console.log("Result", result);
    }
  } catch (err) {
    if (err.response) {
      //   return {
      //     status: "Failed",
      //     error: err.response.data,
      //   };
      console.log(err.response.data);
    }
  }
};
