const axios = require("axios");
// const url = "https://ticketappekaa.azurewebsites.net/";
const {ticketApplication} = require("../config/config");
const url = ticketApplication.baseUrl;

module.exports = {
  getUserDetails: async (email) => {
    try {
      const resp = await axios.get(`${url}/User/getUser/${email}`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },
};
