const axios = require('axios');
// const url = "https://ticketappekaa.azurewebsites.net/";
const {ticketApplication} = require("../config/config");
const url = ticketApplication.baseUrl;
let status;

module.exports.fetchTicketStatusOfUser = async(user_id,ticket_id) => {
    try{
      const response = await axios.get(`${url}/User/${user_id}/allTickets`);
      if(response && response.data){
        let ticketStatus;
        ticketTable = response.data.data;
        ticketTable.map(item => {
          if(item.ticket_id === ticket_id){
            ticketStatus = item.ticketTable.ticket_state
          }
        })
        if(ticketStatus){
          switch(ticketStatus){
            case "1" : status = "New"
            break;
             
            case "2" : status = "In Progress"
            break;
      
            case "3" :status = "Onhold"
            break;
  
            case "4" :status = "Resolved"
            break;
  
            case "5" :status = "Cancel"
            break;
            
          }
          return {
            status : "Success",
            data : status
          }
        }else{
           return {
            status : "Failed",
            data : "No Ticket Exist with this id"
          }
        }
      }
    }catch(error){
      console.log(error)
    }
  
  }