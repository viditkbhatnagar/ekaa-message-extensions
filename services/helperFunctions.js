const config = require('../config/config')

async function sendProactiveMessages(userId, conversationId, aadobjectId,tenantId) {
  let managerReference;
  managerReference = {
    user: {
      id: userId,
      aadObjectId: aadobjectId,
    },
    bot: { id: `28:${config.Bot.MicrosoftAppId}` },
    conversation: {
      conversationType: "personal",
      tenantId: tenantId,
      id: conversationId,
    },
    channelId: "msteams",
    locale: "en-US",
    serviceUrl: "https://smba.trafficmanager.net/apac/",
  };

return managerReference
}

module.exports.sendProactiveMessages = sendProactiveMessages;

