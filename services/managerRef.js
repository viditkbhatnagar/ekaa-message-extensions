const axios = require("axios");
const url = "https://ticketappekaa.azurewebsites.net/";

module.exports = {
  saveManagerReference: async (email, reference_info) => {
    try {
      console.log({
        email,
        reference_info,
      });
      if (email && reference_info) {
        const response = await axios.put(`${url}/User/${email}/addReference`, {
          manager_reference: reference_info,
        });
        console.log("done");
        return {
          status: "Success",
          message: "Reference saved successfully",
        };
      }
    } catch (error) {
      if (error.response) {
        return {
          status: "Failed",
          error: error.response.data,
        };
      }
    }
  },

  getManagerReference: async (email) => {
    try {
      if (email) {
        const response = await axios.get(`${url}/User/${email}/getReference`);
        if (response && response.data) {
          // console.log(response.data);
          return response.data.data;
        }
      }
    } catch (error) {
      if (error.response) {
        console.log(error.response.data);
      }
    }
  },
};
