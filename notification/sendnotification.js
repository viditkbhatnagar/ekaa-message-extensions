// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const {
    DialogSet,
    WaterfallDialog,
    DialogTurnStatus,
    ComponentDialog,
  } = require("botbuilder-dialogs");
  const { CardFactory, MessageFactory } = require("botbuilder");
  const MAIN_DIALOG = "sendNotification";
  const MAIN_WATERFALL_DIALOG_NOTIFY = "MainWaterfallDialogToDoNotify";
//   const { Notification, notificationApproved } = require("../cards/card");
const {agentMicroAnalytics} = require("../utilities/cards");

  class sendNotification extends ComponentDialog {
    constructor(conversationState, data) {
      super(MAIN_DIALOG);
      this.conversationDataAccessor = conversationState;
      this.data = data;
      this.addDialog(
        new WaterfallDialog(MAIN_WATERFALL_DIALOG_NOTIFY, [
          this.initNotify.bind(this),
        ])
      );
      this.initialDialogId = MAIN_WATERFALL_DIALOG_NOTIFY;
    }
    async run(turnContext, accessor) {
      const dialogSet = new DialogSet(accessor);
      dialogSet.add(this);
      const dialogContext = await dialogSet.createContext(turnContext);
      const results = await dialogContext.continueDialog();
      if (results.status === DialogTurnStatus.empty) {
        return await dialogContext.beginDialog(this.id);
      }
    }
    async initNotify(stepContext) {
      let replyActivity= MessageFactory.attachment(
          CardFactory.adaptiveCard(agentMicroAnalytics(
              this.data
          ))
        );
     
      replyActivity.summary = "Eka Bot";
      replyActivity.channelData = {
        notification: {
          alert: true,
        },
      };
      await stepContext.context.sendActivity(replyActivity);
      return await stepContext.endDialog(DialogTurnStatus.complete);
    }
  }
  module.exports.sendNotification = sendNotification;