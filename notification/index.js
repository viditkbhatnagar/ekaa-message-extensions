
const sendNotification = require("./sendnotification").sendNotification;
const axios = require("axios");
const {getAgentDetails} = require('../apiFunctions/index');
// const { sendProactiveMessages,getManagerReference,getCallerEmail } = require("../apiFunctions/");
const {getManagerReference} = require('../services/index')

class Notify {
  constructor(adapter, conversationState) {
    this.adapter = adapter;
    this.conversationState = conversationState;
    this.dialogState =
      this.conversationState.createProperty("DialogStateNotify");
  }
  async createNotify(body) {
      try {
        const recipient = body;
        // let res = await cosmos.Find([recipient]);
        const res = await getManagerReference(
            body
          );
          // res['channelId'] = "msteams";
          console.log("response-==",res.data);
        this.adapter.continueConversation(
          res.data.conversation,
          async (turnContext) => {
            try {

                const data = await getAgentDetails(body);
                if (data.status == "Success") {
                  console.log("here");
                  const ticket = {   
                  inprogress : data.inprogress_ticket,
                  closed : data.onhold_ticket,
                  onhold : data.closed_ticket,
                  newticket : data.new_ticket,
                  }
              const dialog = new sendNotification(this.conversationState, ticket);
              await dialog.run(turnContext, this.dialogState);
                } 
            } catch (err) {
              console.log("error on inner notification", err);
            }
          }
        );
      } catch (err) {
        console.log("error on notification", err);
      }
    
  }
}
module.exports.Notify = Notify;

// response-== {
//   email: 'shuvam.sengupta@celebaltech.com',
//   userId: '29:1NEVlNoIJxOx14loZENXsGutK5-AhVdJWmIYmArYXRcxAxFHqvMYxHoykHM8vSPa2iruA7coQsNyWJ8Eq_4xPZw',
//   convId: 'a:12JxSsyPFrz0SHTOVRLeHKbjZ22eARUVCi3u3qhP7F-WklpMbaCNkmgMkvOhOOYjv5ofGyZ8so2j1iN7bJee7CIckASUE435AT5IP4jIyFCJR6UXxJV_6oBcX21WREFo2',
//   aadObjectId: 'fd39393d-5e20-4e63-93d8-b3aef545d062',
//   tenantID: 'e4e34038-ea1f-4882-b6e8-ccd776459ca0'
// }