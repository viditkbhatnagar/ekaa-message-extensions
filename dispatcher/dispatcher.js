const {
  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog,
  OAuthPrompt,
  ConfirmPrompt,
} = require("botbuilder-dialogs");
const CronJob = require("cron").CronJob;
const { CardFactory, TeamsInfo, TurnContext } = require("botbuilder");
const { UserDialog } = require("../dialogs/userFlow/mainUserDialog");
const {
  AgentDispatch,
  AgentMainD,
  AgentMicroAnalytics,
} = require("../dialogs/agentFlow");
const {
  checkIsAgent,
  getUserDetails,
  getAgentDetails,
} = require("../apiFunctions");
const { DailogConst } = require("../utilities/constants");
const config = require("../config/config");
const { v1: uuidv1 } = require("uuid");
// const { agentProfile, userProfile} = require("../stateManager/userProfile");
const { UserProfile } = require("../stateManager/userProfile");
const { AgentProfile } = require("../stateManager/agentProfile");
const { Analytics } = require("../cron/getAgentAnalytics");
const cron = require("node-cron");
const { saveManagerReference } = require("../services/managerRef");
const {
  sendProactiveMessages,
  getManagerReference,
  getCallerEmail,
} = require("../services/");
const { Bot } = require("../bot/bot");
const { CronCaller } = require("../cron/cronCaller");
const { agentMicroAnalytics } = require("../utilities/cards/");
// const { createChat } = require("../MicrosoftGraph/client");
const { createChat } = require("../MicrosoftGraph/chat/api");
const OAUTH_PROMPT = "OAuthPrompt";
const CONFIRM_PROMPT = "ConfirmPrompt";
let BotContext;
let token;

class Dispatcher extends ComponentDialog {
  constructor(conversationState, userState) {
    super(DailogConst.DISPATCHER_DIALOG);

    if (!conversationState) {
      throw new Error(
        "[Dispatcher]: Missing parameter. conversationState is required"
      );
    }

    if (!userState) {
      throw new Error(
        "[Dispatcher]: Missing parameter. user State is required"
      );
    }
    this.userState = userState;
    this.conversationState = conversationState;

    this.UserProfileAccessor = userState.createProperty("USER");
    this.AgentProfileAccessor = userState.createProperty("AGENT");

    this.addDialog(
      new OAuthPrompt(OAUTH_PROMPT, {
        connectionName: "fresh",
        text: "Please Sign In",
        title: "Sign In",
        timeout: 300000,
      })
    );
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(new UserDialog(conversationState, userState));
    this.addDialog(new AgentDispatch(conversationState, userState));
    this.addDialog(new AgentMainD(conversationState, userState));
    this.addDialog(new AgentMicroAnalytics(conversationState));

    this.addDialog(
      new WaterfallDialog(DailogConst.DISPATCHER_WF, [
        // this.promptStep.bind(this),
        // this.loginDialog.bind(this),
        this.dispatcherStep.bind(this),
      ])
    );

    this.initialDialogId = DailogConst.DISPATCHER_WF;
  }

  async promptStep(stepContext) {
    console.log("Step 1");
    return await stepContext.beginDialog(OAUTH_PROMPT);
  }

  async loginDialog(stepContext) {
    console.log("Step 2");
    const tokenResponse = stepContext.result;
    if (tokenResponse) {
      await stepContext.context.sendActivity("You are now logged in.");
      console.log("Token : ", tokenResponse);
      // await createChat(tokenResponse.token);
    } else {
      await stepContext.context.sendActivity("Login was not successful.");
    }
    return await stepContext.next();
  }

  async dispatcherStep(stepContext) {
    //ADD USER STATE

    await this.addUserREF(stepContext);
    // BotContext = stepContext.context;

    const member = await TeamsInfo.getMember(
      stepContext.context,
      stepContext.context.activity.from.id
    );
    let email = member.email;

    // let email = "akshi.atreya@celebaltech.com";
    let persona = await checkIsAgent(email);
    console.log(persona);
    if (persona) {
      console.log("Switching to Agent");

      let agentDetails = await getAgentDetails(email);
      console.log(agentDetails);
      if (agentDetails.status == "Success") {
        //Save Agent Details
        let agentData = await this.AgentProfileAccessor.get(
          stepContext.context,
          new AgentProfile()
        );
        let userData = await this.UserProfileAccessor.get(
          stepContext.context,
          new UserProfile()
        );

        agentData.AGENT_ID = agentDetails.data.data.agent_id;
        agentData.AGENT_EMAIL = agentDetails.data.data.email;
        agentData.AGENT_F_NAME = agentDetails.data.data.first_name;
        agentData.AGENT_L_NAME = agentDetails.data.data.last_name;
        agentData.AGENT_TOTAL_TICKET = agentDetails.data.data.tickets_assigned;
        //Save User Details (As Agent is also a User)
        agentData.AGENT_DEPT = agentDetails.data.data.department;
        userData.USER_ID = agentDetails.data.data.user_id;
        userData.USER_EMAIL = agentDetails.data.data.email;
        userData.USER_F_NAME = agentDetails.data.data.first_name;
        userData.USER_L_NAME = agentDetails.data.data.last_name;
        console.log(agentData);
        console.log(userData);
      } else {
        console.log(data.error);
      }

      return await stepContext.beginDialog(DailogConst.AGENT_DISPATCHER_DIALOG);
    } else {
      console.log("Switching to User");
      let userDetails = await getUserDetails(email);
      console.log(userDetails);
      if (userDetails.status == "Success") {
        let userData = await this.UserProfileAccessor.get(
          stepContext.context,
          new UserProfile()
        );
        userData.USER_ID = userDetails.data.data.user_id;
        userData.USER_EMAIL = userDetails.data.data.email;
        userData.USER_F_NAME = userDetails.data.data.first_name;
        userData.USER_L_NAME = userDetails.data.data.last_name;
        console.log(userData);
      } else {
        console.log(userDetails.error);
      }

      return await stepContext.beginDialog(DailogConst.USER_MAIN_DIALOG);
    }
  }

  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  async addUserREF(stepContext) {
    const conversationReference = TurnContext.getConversationReference(
      stepContext.context.activity
    );
    //console.log(conversationReference);
    const member = await TeamsInfo.getMember(
      stepContext.context,
      stepContext.context.activity.from.id
    );
    let email = member.email;

    let reference_info = {
      email: email,
      userId: conversationReference.user.id,
      convId: conversationReference.conversation.id,
      aadObjectId: conversationReference.user.aadObjectId,
      tenantID: conversationReference.conversation.tenantId,
      data: {
        conversationReference,
      },
    };

    const saveReference = await saveManagerReference(email, reference_info);
    if (saveReference.status == "Success") {
      console.log("Saved");
    } else {
      console.log(saveReference.error);
    }
  }

  async onContinueDialog(innerDc) {
    try {
      const result = await this.interrupt(innerDc);
      if (result) {
        return result;
      }
      return await super.onContinueDialog(innerDc);
    } catch (err) {
      console.log(err);
    }
  }
  async interrupt(innerDc) {
    try {
      console.log("interrrupt====", innerDc.context.activity);
      innerDc.context.activity.value
        ? (innerDc.context.activity.text = innerDc.context.activity.value.action
            ? innerDc.context.activity.value.action
            : innerDc.context.activity.text)
        : (innerDc.context.activity.text = innerDc.context.activity.text);
      if (innerDc.context.activity.text) {
        let text = innerDc.context.activity.text;

        switch (text) {
          // case "help":
          // case "?": {
          //   const helpMessageText =
          //     "How may i help you ? Please describe your problem below and hit send.";
          //   await innerDc.context.sendActivity(
          //     helpMessageText,
          //     helpMessageText,
          //     InputHints.ExpectingInput
          //   );
          //   return { status: DialogTurnStatus.waiting };
          // }
          // case "quit": {
          //   console.log("quit is working......");
          //   return await innerDc.cancelAllDialogs();
          // }
          // case "hi":
          // case "hello":
          // case "Hi":
          // case "Home":
          case "#SWITCH":
          case "#Persona":
          case "#switch": {
            await innerDc.cancelAllDialogs();
            return innerDc.replaceDialog(DailogConst.DISPATCHER_DIALOG);
          }
          case "#Agent":
          case "#AGENT":
          case "#agent": {
            await innerDc.cancelAllDialogs();
            return await innerDc.beginDialog(DailogConst.AGENT_MAIN_DIALOG);
          }
          case "#USER":
          case "#User":
          case "#user": {
            console.log("inside Dispatcher #user");
            await innerDc.cancelAllDialogs();
            return await innerDc.beginDialog(DailogConst.USER_MAIN_DIALOG);
          }
        }
      }
    } catch (err) {
      console.log(err);
    }
  }
}
module.exports.Dispatcher = Dispatcher;
