const axios = require("axios");
const { ticketApplication } = require("../../../config/config");
const url = ticketApplication.baseUrl;

module.exports.raiseTicket = async (payload, user_id) => {
  // console.log("user API", user_id, payload);
  try {
    const response = await axios.post(`${url}/ticket/${user_id}`, payload);
    console.log(response);
    if (response && response.data) {
      console.log(response.data);
      return {
        status: "Success",
        data: response.data,
      };
    }
  } catch (error) {
    if (error.response) {
      return {
        status: "Failed",
        error: error.response.data,
      };
    }
  }
};
