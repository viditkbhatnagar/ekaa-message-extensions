const axios = require('axios');
const {ticketApplication} = require("../../../config/config");
const url = ticketApplication.baseUrl;

module.exports.agentAddComment = async (payload, agentid, ticketid) => {
  console.log("agentAddComment",payload,agentid,ticketid);
  try {
    const response = await axios.put(
      `${url}/Agent/${agentid}/ticket/${ticketid}/comment`,
      payload
    );
      if (response && response.data) {
        console.log(response.data);
        return {
          status: "Success",
          data: response.data,
        };
      }
    } catch (error) {
      if (error.response) {
        return {
          status: "Failed",
          error: error.response.data,
        };
      }
    }
  };