const axios = require('axios');
const {ticketApplication} = require("../../../config/config");
const url = ticketApplication.baseUrl;

module.exports.updateTicket = async (payload,agent_id,ticket_id) => {
    try {
      let response;
      if(payload.status === "In Progress"){
        response = await axios.put(`${url}/Agent/${agent_id}/ticket/${ticket_id}/inProgressTicket`);
      }else if(payload.status === "On Hold"){
        response = await axios.put(`${url}/Agent/${agent_id}/ticket/${ticket_id}/onHoldTicket`,{
            on_hold_reason_notes : payload.note
        });
      }else if(payload.status === "Resolved"){
        response = await axios.put(`${url}/Agent/${agent_id}/ticket/${ticket_id}/resolveTicket`,{
            resolution_notes : payload.note
        });
      }else{
        response = await axios.put(`${url}/Agent/${agent_id}/ticket/${ticket_id}/cancelTicket`,{
            cancelation_notes : payload.note
        });
      }
        if(response && response.data){
            console.log("updateTicketStatus",response.data);
            return {
                status: "Success",
                data: response.data
            }
        }
    } catch (error) {
      if (error.response) {
        return {
          status: "Failed",
          error: error.response.data,
        };
      }
    }
  };