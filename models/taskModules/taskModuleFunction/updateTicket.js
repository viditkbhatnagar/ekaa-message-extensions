const axios = require('axios');
const {ticketApplication} = require("../../../config/config");
const url = ticketApplication.baseUrl;

module.exports.updateTicket = async (payload,userid,ticketId) => {
    try {
      const response = await axios.put(
        `${url}/User/${userid}/ticket/${ticketId}`,
        payload
      );
      if (response && response.data) {
        // console.log(response.data);
        return {
          status: "Success",
          data: response.data,
        };
      }
    } catch (error) {
      if (error.response) {
        return {
          status: "Failed",
          error: error.response.data,
        };
      }
    }
  };