const { UISettings } = require('./uisettings');
const { TaskModulesIds } = require('./taskmoduleid');

const TaskModuleUIConstants = {
    RAISE_TICKET : new UISettings(700, 530, 'Raise Ticket', TaskModulesIds.RAISE_TICKET, 'RAISE_TICKET'),
    CHECK_TICKET_STATUS : new UISettings(700, 530, 'Check ticket status', TaskModulesIds.CHECK_TICKET_STATUS, 'CHECK_TICKET_STATUS'),
    SHOW_TICKETS_TO_AGENT : new UISettings(700, 530, 'All Assigned Tickets', TaskModulesIds.SHOW_TICKETS_TO_AGENT, 'SHOW_TICKETS_TO_AGENT'),
}

module.exports.TaskModuleUIConstants = TaskModuleUIConstants