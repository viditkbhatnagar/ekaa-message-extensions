const winston = require("winston");

module.exports.logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "./logger/managerRef.js",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),

    new winston.transports.File({
      level: "error",
      filename: "./logger/error.js",
      json: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),

    // new winston.transports.File({
    //   level: "error",
    //   filename: "./apiError.log",
    //   json: true,
    //   format: winston.format.combine(
    //     winston.format.timestamp(),
    //     winston.format.json()
    //   ),
    // }),
  ],
});
